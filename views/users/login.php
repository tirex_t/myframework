<?php $title_for_layout = "Page de connexion"; ?>
<div id="slides-container" class="slides-container">
        <div id="slides">
            <div>
                <div class="slide-image"><img src="../images/slideemmoo-1.png" alt="Slide #1 image" /></div>
                <div class="slide-text">
                    <!-- **************************************** SLIDER *************************************************** -->
                    <h2>Slide #1</h2>
                    <p class="frontpage-button">
                    	<a href="#">Lien vers la page</a>
                    </p>
                </div>
            </div>
            <!-- **************************************** SLIDER *************************************************** -->
            <div>
            	<h2>Slide #2 Images a mettre en avant!</h2>
                <p>On peut mettre ici indifféremment des images, du texte ou autre..</p>
            </div>
            <!-- **************************************** SLIDER *************************************************** -->
            <div>
                <div class="slide-image slide-image-right"><img src="../images/slideemmoo-3.png" alt="Slide #3 image" /></div>
                <div class="slide-text">
                    <h2>Slide #3 image et texte</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque venenatis sagittis enim. Maecenas ligula erat, egestas congue, varius nec, sagittis nec, purus. In neque. Curabitur at metus tincidunt dui tristique molestie. Donec porta molestie tortor. Fusce euismod consectetuer sapien. Fusce ac velit.</p>
                </div>
            </div>
		</div>
        <div class="controls"><span class="jFlowNext"><span>Suivant</span></span><span class="jFlowPrev"><span>Precedent</span></span></div>        
        <div id="myController" class="hidden"><span class="jFlowControl">Slide 1</span><span class="jFlowControl">Slide 1</span><span class="jFlowControl">Slide 1</span></div>
    </div>

<div class="login_form">
	<h1>Zone sécurisée</h1>
	<form action="/login" method="post">
		<?php echo $this->Form->input('login','Identifiant'); ?>
		<?php echo $this->Form->input('password','Mot de passe',array( 'type' => 'password' )); ?>
		<div class="Actions">
			<input type="submit" value ="Se connecter">
		</div>
	</form>
</div>