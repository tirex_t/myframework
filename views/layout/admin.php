<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <title><?php echo isset($title_for_layout)?$title_for_layout:'Le Taxi-Brousse Interface d\'administration'; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="back-office">
    <meta name="author" content="Chromatiq Design">

    
    <!-- Core CSS - Include with every page -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="/admin/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="/admin/css/plugins/timeline/timeline.css" rel="stylesheet">

    <link href="/admin/css/sb-admin.css" rel="stylesheet">
    <link href="/admin/css/styles.css" rel="stylesheet">
    <link href="/admin/css/prettyPhoto.css" rel="stylesheet">
    <link href="/admin/css/avgrund.css" rel="stylesheet">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Router::url('cockpit'); ?>">Administration Le Taxi-Brousse</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li><a target="_blank" href="<?php echo Router::url(''); ?> "><i class="fa fa-external-link-square"></i>  Voir le site</a> </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Messages
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <h5>Les 3 dernieres reservations</h5>
                        <?php foreach ($inscriptions as $inscr): ?>
                            <li>
                                <a href="">
                                    <div>
                                        <strong><?php echo $inscr->nom.' '.$inscr->prenom; ?></strong>
                                        <span class="pull-right text-muted">
                                            <em><?php echo frDate($inscr->datecreation); ?> </em>
                                        </span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                        <?php endforeach ?>
                        <li>
                            <a class="text-center" href=" <?php echo Router::url('admin/user_index'); ?> ">
                                <strong>Voir toutes les demandes</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $user->nom; ?>
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Votre compte</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo Router::url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i>Déconnexion</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Rechercher...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="<?php echo Router::url('cockpit'); ?>"><i class="fa fa-dashboard fa-fw"></i> Tableau de bord</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-folder-open-o fa-fw"></i>Gestion du Site<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Router::url('admin/music_edit'); ?>">Liste de lecture musicale</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('admin/produit_index'); ?>">Offres spéciales</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-folder-open-o fa-fw"></i> Gestion du restaurant<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Router::url('admin/catmenu_index'); ?>">Categories de menus</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('admin/menu_index'); ?>">Plats</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('admin/plat_edit'); ?>">Ajouter un plat</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('admin/menu_edit'); ?>">Composer un menu</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-folder-open-o fa-fw"></i> Gestion des réservations<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Router::url('admin/resa_index'); ?>">Lister les reservations en cours</a>
                            </li>                            
                            <li>
                                <a href="<?php echo Router::url('admin/sousmenu_index'); ?>">Valider les réservations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-folder-open-o fa-fw"></i> Gestion des commandes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Router::url('admin/media_index'); ?>">Ventes à emporter en cours</a>
                            </li>                            
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i>Administrateur<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Router::url('/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i>Se déconnecter</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            
                <?php if ($this->session->hasFlash()) {
                    echo $this->session->flash();
                } ?>
            

            <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo Router::url('cockpit'); ?>">Dashboard</a></li>
                        <li class="active"><?php echo $breadcrumb; ?></li>
                    </ol>

                    <div class="page-header entetepage">Votre interface d'administration</div>
            </div>

            <?php echo $content; ?>
        </div>
        
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Core Scripts - Include with every page -->
    <script src="/Admin/js/jquery-1.10.2.js"></script>
    <script src="/Admin/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="/Admin/js/tinymce/tinymce.min.js"></script>
    <script src="/Admin/js/bootstrap.min.js"></script>
    <script src="/Admin/js/jquery.prettyPhoto.js"></script>
    <script src="/Admin/js/jquery.form.js"></script>
    <script src="/Admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/Admin/js/plugins/avgrund/jquery.avgrund.min.js"></script>
    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="/Admin/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <!-- // <script src="/Admin/js/plugins/morris/morris.js"></script> -->
    <!-- SB Admin Scripts - Include with every page -->
    <script src="/Admin/js/sb-admin.js"></script>
    <script src="/Admin/js/custom.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            selector: "textarea",
            theme: "modern",
            language : "fr_FR",
            plugins: [
                "advlist autolink lists link charmap preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime nonbreaking table directionality",
                "emoticons paste textcolor"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link ",
            toolbar2: "print preview media",
            image_advtab: false,
        });

    </script>
</body>

</html>