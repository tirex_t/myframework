<div class="row">
  <div class="col-lg-8">
    <h2><?php echo "Gestion des musiques du site"; ?> </h2>
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped tablesorter">
        <thead>
          <tr>
            <th>Actions </th>
            <th>Titre <i class="fa fa-sort"></i></th>
            <th>Date d'ajout<i class="fa fa-sort"></i></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($menus as $menu): ?>
            <tr>
              <td>
                <a href=""><i class="fa fa-trash-o"></i></a>
              </td>
              <td><?php echo $menu->titre; ?> </td>
              <td><?php echo frDate($menu->datemodif); ?></td>
            </tr>
          <?php endforeach ?>
          <tr>
            <td colspan="5"><a href="<?php echo Router::url('admin/music_edit/') ?>" id="add_zik"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Ajouter une chanson</a> </td>
          </tr>
        </tbody>
      </table>
    </div>

	<div id="uploadzik">
		<form id="formulaire" action="<?php echo Router::url('admin/music_edit/'); ?>" method="post">
			<div class="row">
        
        <span class="btn btn-default btn-file">
            Choisir la fichier mp3 à ajouter <input id="musicfile" type="file">
        </span>
        <div id="select_file"></div>
			</div>

			<div class="actions">
				<button type="reset" class="btn btn-inverse btn-large btn_reset">Annuler <i class="fa fa fa-times"></i></button>
				<button type="submit" class="btn btn-success btn-large btn_submit">Enregistrer  <i class="fa fa fa-check"></i></button>	
			</div>
	</form>
    </div>
  </div>
  <div class="col-lg-4">
    <h2>Guide de saisie</h2>
    <p>Pour éditer un article, cliquez sur le bouton à gauche de la ligne </p>
    <p>Pour ajouter un article, cliquez sur le bouton en dessous du tableau ou ici</p>
  </div>
</div>