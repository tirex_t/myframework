<?php $title_for_layout = "Taxi-Brousse - Page d'administration."; ?>



    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-exclamation-circle"></i>  Les dernières statistiques</h1>
        </div>
    </div>
      <!-- /.row -->
    <div class="row stats">
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Réservations non validées
                </div>
                <div class="panel-body">
                    
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Messages contacts recus
                </div>
                <div class="panel-body">
                    7
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Menus
                </div>
                <div class="panel-body">
                    123
                </div>
            </div>
        </div>
    </div>
    <div class="row stats">
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dernières connexions
                </div>
                <div class="panel-body">
                    
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visites du site
                </div>
                <div class="panel-body">
                    7
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Durée moyenne de la visite
                </div>
                <div class="panel-body">
                    4min25s
                </div>
            </div>
            </div>
     </div>
     <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-table"></i> Les derniers menus saisis</h1>
        </div>
          <div class="table-responsive col-lg-12">
             <table class="table table-striped table-bordered table-hover table-locations" id="dataTables-locations">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th> Titre</th>
                        <th>Description</th>
                        <th>Categorie</th>
                        <th>Date dernière modification</th>
                    </tr>
                </thead>
                <tbody>
                        <?php foreach ( $produits as $key => $value): ?>
                            <tr class="odd gradeX"> 
                                <td><a href="<?php echo Router::url('admin/produit_edit/'.$value->id);?>" class=""><i class="fa fa-edit"></i></a><a href="<?php echo Router::url('admin/produit_delete/'.$value->id); ?>" class=""><i class="fa fa-trash-o"></i></a></td>
                                <td><?php echo $value->titre; ?></td>
                                <td><?php echo reduireChaine($value->description,30); ?></td>
                                <td ><?php echo $value->categorie; ?></td>
                                <td ><?php echo frDate($value->datemodif); ?></td>
                            </tr>
                        <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan='5'><a href="<?php echo Router::url('admin/produit_index'); ?> "><i class="fa fa-list"></i>&nbsp;&nbsp;&nbsp;&nbsp;Voir tous les produits</a> </td>
                    </tr>
                </tfoot>
             </table>
        </div>
     </div>