<div class="row">
  <div class="col-lg-8">
    <h2>Reservations en cours</h2>
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped tablesorter">
        <thead>
          <tr>
            <th>Actions </th>
            <th>Client <i class="fa fa-sort"></i></th>
            <th>Date<i class="fa fa-sort"></i></th>
            <th>Heure<i class="fa fa-sort"></i></th>
            <th>Message<i class="fa fa-sort"></i></th>
            <th>Couverts <i class="fa fa-sort"></i></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ( $resas as $resa ): ?>
            <tr>
              <td>
                <a href="<?php echo Router::url('admin/resa_valid/'.$resa->id); ?> "><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo Router::url('admin/resa_delete/'.$resa->id); ?>"><i class="fa fa-trash-o"></i></a>
              </td>
              <td><?php echo $resa->nomclient.' '.$resa->prenomclient; ?> </td>
              <td><?php echo frDate($resa->dateresa); ?></td>
              <td><?php echo $resa->heureresa; ?></td>
              <td><?php echo $resa->messageclient; ?></td>
              <td><?php echo $resa->couverts; ?></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-lg-4">
    <h2>Guide de saisie</h2>
    <p>Pour éditer un article, cliquez sur le bouton à gauche de la ligne </p>
    <p>Pour ajouter un article, cliquez sur le bouton en dessous du tableau ou ici</p>
  </div>
</div>