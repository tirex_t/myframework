<?php $title_for_layout = "Le Taxi-Brousse - Edition des categories de menus"; ?>
<h1 class="page-header"><?php echo $breadcrumb; ?></h1>
<?php $arr_menus = array(); ?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-7">
						<form action="<?php echo Router::url('admin/catmenu_index/'); ?>" method="post" id="form_type">
								<?php echo $this->form->input('id','hidden'); ?>								
								<?php echo $this->form->input('type','Nouvelle categorie de menu :'); ?>
								
								<div class="actions">
									<button type="reset" class="btn btn-inverse btn-large btn_emmo_reset">Annuler <i class="fa fa fa-times"></i></button>
									<button type="submit" class="btn btn-success btn-large btn_emmo_submit">Enregistrer  <i class="fa fa fa-check"></i></button>									
								</div>
						</form>
					</div>
					<div class="col-lg-5 sousmenu">
						<div class="panel panel-default">
							<div class="panel-heading">
								Ici vous pouvez modifier les categories de menus. </br>	
							</div>
							<div class="panel-body">
								Actuellement, vous avez pour le menu  : </br>
								<?php foreach ($menulocations as $menu): ?>
									<a class="sousmenu_type" rel="<?php echo $menu->id; ?>" href=""><?php echo $menu->type;?></a><br>
									<?php if(!in_array($menu->id, $arr_menus)) $arr_menus[] = $menu->id; ?>
								<?php endforeach ?>
								<?php echo count($menulocations); ?> sous-menus.</br>								
							</div>
							<div class="panel-body">
								Et pour le menu vente :</br>
								<?php foreach ($menuventes as $menu): ?>
									<a class="sousmenu_type" href="" rel="<?php echo $menu->id; ?>"><?php echo $menu->type; ?> </a><br>
									<?php if(!in_array($menu->id, $arr_menus)) $arr_menus[] = $menu->id; ?>
								<?php endforeach ?>
								<?php echo count($menuventes); ?> sous-menus </br>								
							</div>
							<div class="panel-body">
								Menus inutilisés</br>
								<?php if (count($menus)==count($arr_menus)): ?>
									0 menu inutilisé
								<?php else :?>
								<?php foreach ($menus as $menu): ?>
									<?php if (!in_array($menu->id, $arr_menus)): ?>
										<a class="sousmenu_type" href="" rel="<?php echo $menu->id; ?>"><?php echo $menu->libelle; ?> </a><br>
									<?php endif ?>
								<?php endforeach ?>
									<?php endif ?>
							</div>

						</div>
						<div class="row">
							Cliquez sur un des sous menus si vous souhaitez le modifier.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
