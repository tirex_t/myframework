<div class="row">
  <div class="col-lg-8">
    <h2>Liste des menus</h2>
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped tablesorter">
        <thead>
          <tr>
            <th>Actions </th>
            <th>Nom du menu <i class="fa fa-sort"></i></th>
            <th>Categorie menu <i class="fa fa-sort"></i></th>
            <th>Description <i class="fa fa-sort"></i></th>
            <th>Date derniere modification<i class="fa fa-sort"></i></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ( $menus as $menu ): ?>
            <tr>
              <td>
                <a href="<?php echo Router::url('admin/menu_edit/'.$menu->id); ?> "><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo Router::url('admin/menu_delete/'.$menu->id); ?>"><i class="fa fa-trash-o"></i></a>
              </td>
              <td><?php echo $menu->titre; ?> </td>
              <td><?php echo $menu->categorie; ?></td>
              <td><?php echo $menu->description; ?></td>
              <td><?php echo frDate($menu->datemodif); ?></td>
            </tr>
          <?php endforeach ?>
          <tr>
            <td colspan="5"><a href="<?php echo Router::url('admin/menu_edit/') ?>"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Ajouter un menu</a> </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-lg-4">
    <h2>Guide de saisie</h2>
    <p>Pour éditer un article, cliquez sur le bouton à gauche de la ligne </p>
    <p>Pour ajouter un article, cliquez sur le bouton en dessous du tableau ou ici</p>
  </div>
</div>