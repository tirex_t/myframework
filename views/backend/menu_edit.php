<h1><?php echo $breadcrumb; ?></h1>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo $breadcrumb; ?>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-7">
						<form action="<?php echo Router::url('admin/menu_edit/'.$id); ?>" method="post">
								<?php echo $this->form->input('id','hidden'); ?>
								<?php echo $this->form->input('type_id','Type de menu',array('type' => 'select','options' => $typesmenus )); ?>

								<?php echo $this->form->input('nom','Nom du menu'); ?>
								<div class="form_group">
									<label for="">Contenu</label>
									<textarea name="description" class="wysiwyg" cols="95" rows="15"></textarea>
								</div>

								<div class="row">
									<div id="plupload">
										<div id="droparea">
											<p>Déposez les photos à ajouter ici.</p>
											<span class="or">ou</span>
											<a href="#" id="browse">Parcourir</a>
										</div>
										<div id="filelist">
											<?php if ($images): ?>
												<?php foreach ($images as $v ): ?>
													<div class="pl-file">
														<img src="<?php echo $v['url'];?>" />
														<?php echo $v['titre']; ?>
														<div class="img_actions">
															<a class="del" href="<?php echo $v['url']; ?> ">Supprimer</a>
														</div>
													</div>
												<?php endforeach ?>
											<?php endif ?>
										</div>
										<!-- <a href="#" id="upload">Envoyer</a> -->
									</div>
								</div>
					
								<div class="actions">
									<button type="submit" class="btn btn-default btn-primary">Enregistrer</button>
									<button type="reset" class="btn btn-default ">Annuler tout</button>
								</div>
						</form>
					</div>
					<div class="col-lg-5">
						Saisie d'un menu:
						Pensez à ajouter au moins une image pour illustrer 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
