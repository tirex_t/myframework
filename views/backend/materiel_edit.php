<h1>Page d'édition du matériel</h1>

	<section id="main" class="column">
		
		<h4 class="alert_info">Bienvenue dans l'interface admin</h4>
		
		<article class="module width_full">
			<header><h3>Les statistiques de votre site</h3></header>
				<article class="stats_overview">

					<div class="overview_today">
						<p class="overview_day">Materiels</p>
						<p class="overview_count">1,876</p>
						
						<p class="overview_type">Inscriptions en attente</p>
						<p class="overview_count">7</p>
						
					</div>
					<div class="overview_previous">
						<p class="overview_day">Utilisateurs</p>
						<p class="overview_count">1,646</p>
						<p class="overview_type">Dernieres connexions</p>
						<p class="overview_count">2,054</p>
						
					</div>
				</article>
				<div class="clear"></div>

		</article><!-- end of stats article -->
		</header>

		</div><!-- end of .tab_container -->
		</article><!-- end of content manager article -->
		</article><!-- end of post new article -->
				</div>
		</article><!-- end of styles article -->
		<div class="spacer"></div>
	</section>
