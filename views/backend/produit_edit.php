<?php $title_for_layout = "Emmoo - Edition d'un produit"; ?>
<h1><?php echo $breadcrumb; ?></h1>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo $breadcrumb; ?>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-7">
						<form action="<?php echo Router::url('admin/produit_edit/'); ?>" method="post">
								<?php echo $this->form->input('id','hidden'); ?>
								<?php echo $this->form->input('type','Type de produit'); ?>
								<?php echo $this->form->input('categorie','Categorie',array('type' => 'radio','options' => array('Location','Vente'))); ?>
								<?php echo $this->form->input('titre','Titre'); ?>
								<?php echo $this->form->input('image','Illustrations', array('type'=>'image','urls' => array('http://dummyimage.com/250x250/4d494d/686a82.gif&text=test','http://dummyimage.com/150x150/4d494d/686a82.gif&text=test'))); ?>
								<?php echo $this->form->input('description','Description',array( 'class'=> 'large wysiwyg','rows' => 15,'cols' => 49,'type' => 'textarea')); ?>
								
								<div class="actions">
									<button type="submit" class="btn btn-default btn-primary">Enregistrer</button>
									<button type="reset" class="btn btn-default ">Annuler tout</button>
								</div>
						</form>
					</div>
					<div class="col-lg-5">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
