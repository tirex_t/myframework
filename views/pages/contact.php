<?php $title_for_layout = "Page de contact"; 

?>

<body>
    <div id="wrapper">
        <div id="inner-wrap">
            <div id="header-wrap" style="background-image:url('images/header-page-resa.png');min-height:250px;">
                <h2 id="title">Contact</h2>
            </div>
        </div>
        <div id="main">
            <div class="presentcontact">
                <h1>Contactez nous </h1>
            </div>
            <div id="content-wrap">
                <div id="content">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2829.605650637521!2d-0.5675657000000158!3d44.8295979!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd5527b5eedb2f35%3A0xff35c69d4c54423d!2s85+Cours+de+la+Marne!5e0!3m2!1sfr!2sfr!4v1397428602799" width="750" height="450" frameborder="0" style="border:0"></iframe></p>

                    <div class="post_content contact-page">
                        <div class="entry">
                            <div class="content">
                                <h2>Notre adresse</h2>
                                <p><strong>85 cours de la Marne</strong><br />
                                    33800 Bordeaux<br />
                            </div>
                            <div class="content">
                                <h2>Téléphone</h2>
                                <p>07 60 86 43 50/ 09 54 62 04 97
                            </div>
                            <div class="content last">
                                <h2>Horaires d'ouverture</h2>
                                <p><strong>Lundi &#8211; au Samedi</strong><br />
                                    10h00 a minuit</p>
                                    <p><strong>Dimanche</strong><br />
                                    11h00 à 23h00
                            </div>
                            <div class="cleaner">&nbsp;</div>
                        </div> 
                        <div class="cleaner">&nbsp;</div>          
                    </div>
                <div class="cleaner">&nbsp;</div>
            </div>
        </div>
    </div><!-- / #main -->
</div><!-- / #inner-wrap -->
