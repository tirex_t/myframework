	<body>
		<div id="wrapper">
			<div id="inner-wrap">
				<div id="header-wrap" style="background-image:url('images/header-page-resa.png');min-height:250px;">
					<h2 id="title">Reservation</h2>				
				</div>
				<div id="main">
					<div id="content-wrap">
						<div id="content">
							<div class="post_content">
								<div class="entry">
									<p>Reservez pour etre certain d'avoir une table. Vous recevrez un mail de confirmation à l'adresse spécifiée. Utilisez le plan 
										contenu dans le mail de confirmation de réservation pour savoir comment arriver à notre restaurant </p>
										<div class='gform_wrapper' id='gform_wrapper_1' >
											<form method='post' id='gform_1'  action='/reservation'>
												<div class='gform_heading'>
													<h3 class='gform_title'>Reservez</h3>
													<span class='gform_description'>Remplissez le formulaire pour effectuer votre réservation.</span>
												</div>
												<div class='gform_body'>
													<ul id='gform_fields_1' class='gform_fields top_label description_below'>
														<li id='field_1_2' class='gfield' >
															<label for='date'>Date souhaitée</label>
															<div >
																<input name="dateresa" id='input_1_2' type='text' value='' class='datepicker medium dmy_dot datepicker_with_icon' tabindex='1'>
																<!-- <input name='input_2' id='input_1_2' type='text' value='' class='datepicker medium dmy_dot datepicker_with_icon' tabindex='1' />  -->
															</div>
														</li>
														<li id='field_1_1' class='gfield' >
															<label for='input_1_1'>Heure souhaitée</label>
															<div class='clear-multi'>
																<div class='gfield_time_hour ginput_container' id='input_1_1'>
																	<input type='text' maxlength='2' name='heureresa' id='input_1_1_1' value='' tabindex='2' /> : <label for='input_1_1_1'>HH</label>
																</div>
															</div>
														</li>
														<li id='field_1_6' class='gfield' >
															<label for='couvertsresa'>Nombre de couverts</label>
															<div >
																<select name='couvertsresa' id='input_1_6'  class='medium gfield_select' tabindex='5' >
																	<option value='1-2' >1-2</option>
																	<option value='2-4' >2-4</option>
																	<option value='4-8' >4-8</option>
																	<option value='8-12' >8-12</option>
																	<option value='12+' >12+</option>
																</select>
															</div>
														</li>
														<li id='field_1_3' class='gfield' >
															<label for='nomresa'>Nom</label>
															<div class='ginput_container' id='input_1_3'>
																<span id='input_1_3_3_container' class='ginput_left'>
																	<input type='text' name='nomresa' id='input_1_3_3' value='' tabindex='6' />
																	<label for='prenomresa'>Prenom</label>
																</span>
																<span id='input_1_3_6_container' class='ginput_right'>
																	<input type='text' name='prenomresa' id='input_1_3_6' value='' tabindex='7' />
																</span>
																<div class='gf_clear gf_clear_complex'></div>
															</div>
														</li>
														<li id='field_1_4' class='gfield' >
															<label for='emailresa'>Email</label>
															<div >
																<input name='emailresa' id='input_1_4' type='text' value='' class='medium'  tabindex='8'   />
															</div>
														</li>
														<li id='field_1_5' class='gfield'>
															<label for='messageresa'>Message</label>
															<div >
																<textarea name='messageresa' id='input_1_5' class='textarea medium' tabindex='9'   rows='10' cols='50'></textarea>
															</div>
														</li>
													</ul>
												</div>
												<div class='gform_footer top_label'> 
													<input type='submit' id='gform_submit_button_1' class='button gform_button' value='Réserver' tabindex='10' />
												</div>
											</form>
											</div>
											<script type='text/javascript'> 
											jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); 
											</script>
											<div class="cleaner">&nbsp;</div>
										</div><!-- / .entry -->
										<div class="cleaner">&nbsp;</div>          
									</div><!-- / .post_content -->

									<div id="sidebar">
										<div class="widget feature-posts" id="wpzoom-feature-posts-3">
											<h3 class="title">Quelques formules</h3>
											<div class="content">
											</div>
										</div>
									</div><!-- / #sidebar -->
									<div class="cleaner">&nbsp;</div>

									</div><!-- / #content -->
								</div>

								</div><!-- / #main -->
							</div><!-- / #inner-wrap -->
