				<div id="header-wrap" style="background-image:url('images/header-page-resa.png');min-height:250px;">
					<h2 id="title">Notre carte</h2>				
				</div>

<div id="main">		
	<div id="featdishes">
		<div class="header-menu">
			<div class="btn">
				<a href="/reservation">Reservez votre table maintenant</a>
			</div>
		</div>
	</div>

	<div id="categories">
		<div class="widget category" >
			<div>
				<h2>Nos entrées</h2>
				<div class="posts">
					<div class="post">
						<a href="" rel="lightbox"  title="Entrees">
							<div class="post-thumb">
								<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
							</div>						
						</a>
						<div class="content">
							<div class="titleprice">
								<h3>Salade Taxi-Brousse</h3>
								<div class="price">8€</div>
								<div class="cleaner">&nbsp;</div>
							</div>
							<div class="excerpt">
								<p>Coeur de palmier, tomates cerise, mais, salade, oignons rouge, ananas, sauce du chef </p>
							</div>
						</div>
						<div class="cleaner">&nbsp;</div>
					</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Club Sandwich">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Club Sandwich" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Verrine à l'avocat</h3>								
							<div class="price">9€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Purée d'avocats frais, thon, tomate, sauce du chef. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="images/menus/thumbs/formules-tb.png" rel="lightbox"  title="Cheeseburger">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Cheeseburger" class="Thumbnail thumbnail " width="100" height="80" />
						</div>
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Accras</h3>
							<div class="price">6€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Accras de morue, Accras de Crevettes ou Accras de boeuf. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Pasta Salad">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Pasta Salad" class="Thumbnail thumbnail " width="100" height="80" />
						</div>
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Brochettes d'alloco à l'Ivoirienne</h3>
							<div class="price">5€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Bananes plantain frites </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Club Sandwich">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Club Sandwich" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3> Assiette d'évasion Exotique</h3>								
							<div class="price">12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Accras de crevettes, de boeuf et de morue!</p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
			</div>
			<div class="cleaner">&nbsp;</div>
			<a href=<?php echo PROJECT_URL; ?>docs/menu.pdf target="_blank">Consulter la carte complète(PDF)</a>
		</div>
	</div>
	<div class="widget category" >
		<div>
			<h2>Nos plats</h2>
		</div>
	</div>
	<div class="widget category" >
		<div>
			<h2>Spécialités Afrique Australe</h2>
			<div class="posts">
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Pesto Pasta">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Pesto Pasta" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Attieké</h3>
							<div class="price">13€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Semoule de manioc cuit à la vapeur avec son poisson ou son poulet braisé (Origine Ivoirienne).</p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Spaghetti with Garlic and Oil">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Spaghetti with Garlic and Oil" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Maffé</h3>
							<div class="price">12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Boeuf au coulis d'arachide. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
			</div>
			<div class="cleaner">&nbsp;</div>
			<a href="/docs/menu.pdf" target="_blank">Consulter la carte complète(PDF)</a>
		</div>
	</div>


	<div class="widget category" >
		<div>
			<h2>Spécialités Camerounaises</h2>
			<div class="posts">
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Spare Ribs with Sour Cream">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
					</div>
				</a>
					<div class="content">
						<div class="titleprice">
							<h3>Poulet DG</h3>								
							<div class="price">15€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt"><p>Cassolette de poulet aux épices du chef, légumes et banane plantain cuits à l'étouffée. </p>
						</div>
					</div>

					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="Beef Steak" class="Thumbnail thumbnail " width="100" height="80" />
					</div>						
				</a>
				<div class="content">
						<div class="titleprice">
							<h3>Ndolè</h3>								
							<div class="price">12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt"><p>Mijoté d'épinards cuits dans son coulis d'arachide fraiches et ses oignons dorés.Au boeuf. Royal +3e(Boeuf+Crevette). </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Hot Chicken Wings">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="Hot Chicken Wings" class="Thumbnail thumbnail " width="100" height="80" />
					</div>						
				</a>
					<div class="content">
						<div class="titleprice">
							<h3>Djap</h3>								
							<div class="price">A partir de 10€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt"><p>2pinards sautés à la tomate et aux oignons. </p>
						</div>
					</div>

					<div class="cleaner">&nbsp;</div>

				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Sauce Tomate </h3>
							<div class="price">A partir de 12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Mijoté de tomate fraiche aux légumes et aux épices du chef. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>				
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>B.H(Beignets Haricots)</h3>
							<div class="price">A partir de 10€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Cassolette d'haricots rouges et ses petits beignets </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3> Poisson braisé</h3>
							<div class="price">A partir de 12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Poisson braisé accompagné de sa sauce(Tilapia ou sole entier selon arrivage) </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Mouton braisé</h3>
							<div class="price">12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Gigot de mouton braisé aux épices du chef. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3> Soya </h3>
							<div class="price">A partir de 12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Brochette de boeuf mariné braisé. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
			</div>
			<div class="cleaner">&nbsp;</div>
			<a href="/docs/menu.pdf" target="_blank">Consulter la carte complète(PDF)</a>
		</div>
	</div>

		<div class="widget category" >
		<div>
			<h2>Spécialités Sénégalaises</h2>
			<div class="posts">
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Spare Ribs with Sour Cream">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
					</div>
				</a>
					<div class="content">
						<div class="titleprice">
							<h3>Yassa Poulet</h3>								
							<div class="price">12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>

						<div class="excerpt"><p>Mijoté de poulet aux oignons citronnés. </p>
						</div>
					</div>

					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="Beef Steak" class="Thumbnail thumbnail " width="100" height="80" />
					</div>						
				</a>
				<div class="content">
						<div class="titleprice">
							<h3>Yassa végétarien</h3>
							<div class="price">10€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt"><p>Mijoté d'oignons citronné au riz </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Hot Chicken Wings">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="Hot Chicken Wings" class="Thumbnail thumbnail " width="100" height="80" />
					</div>						
				</a>
					<div class="content">
						<div class="titleprice">
							<h3>Tchiep Djen</h3>
							<div class="price">A partir de 12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt"><p>Poisson et légumes frais sur son lit de riz caramélisé. </p>
						</div>
					</div>

					<div class="cleaner">&nbsp;</div>

				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3> Tchiep Poulet</h3>
							<div class="price">12€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p> Poulet et légumes frais sur son lit caramélisé. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>				
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Tchiep végétarien</h3>
							<div class="price">10€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Riz caramélisé cuit dans un bouillon de légumes frais </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
			</div>
			<div class="cleaner">&nbsp;</div>
			<a href="/docs/menu.pdf" target="_blank">Consulter la carte complète(PDF)</a>
		</div>
	</div>

	<div class="widget category" >
		<div>
			<h2>Desserts</h2>
			<div class="posts">
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Dessert">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Salade de fruits tropicaux</h3>								
							<div class="price">6€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt"><p>Salade de fruits tropicaux fraichement préparés.</p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
				<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="Dessert" class="Thumbnail thumbnail " width="100" height="80" />
					</div>						
				</a>
					<div class="content">
						<div class="titleprice">
							<h3>Coupe des îles</h3>								
							<div class="price">7€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Boules de sorbets sur lit de fruits exotiques. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Dessert">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Dessert" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Glace Duo</h3>
							<div class="price">5€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Parfums aux choix. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Dessert">
					<div class="post-thumb">
						<img src="images/menus/thumbs/formules-tb.png" alt="Dessert" class="Thumbnail thumbnail " width="100" height="80" />
					</div>						
				</a>
					<div class="content">
						<div class="titleprice">
							<h3>Verrine Taxi-Brousse</h3>
							<div class="price">6€</div>
							<div class="cleaner">&nbsp;</div>
						</div>

						<div class="excerpt"><p>Déguè constitué de lait caillé, de mil, de sucre et de vanille. </p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
			</div>
			<div class="cleaner">&nbsp;</div>
			<a href="/docs/menu.pdf" target="_blank">Consulter la carte complète(PDF)</a>
		</div>
	</div>
<div class="widget category" >
		<div>
			<h2>Boissons</h2>
			<div class="posts">
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="Boissons">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="Boisson" class="Thumbnail thumbnail " width="100" height="80" />
						</div>						
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Cocktails exotiques sans alcool</h3>
							<div class="price">A partir de 5€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p> Bissap , Djindja, Exotica</p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
				<div class="post">
					<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" rel="lightbox"  title="">
						<div class="post-thumb">
							<img src="images/menus/thumbs/formules-tb.png" alt="" class="Thumbnail thumbnail " width="100" height="80" />
						</div>
					</a>
					<div class="content">
						<div class="titleprice">
							<h3>Cocktails exotiques avec alcool</h3>
							<div class="price">A partir de 5€</div>
							<div class="cleaner">&nbsp;</div>
						</div>
						<div class="excerpt">
							<p>Punch COCO, Punch d'Amour, Plunch planteur,etc.</p>
						</div>
					</div>
					<div class="cleaner">&nbsp;</div>
				</div>
			</div>
			<div class="cleaner">&nbsp;</div>
		</div>
	</div>
</div>
	</div><!-- / #main -->
</div><!-- / #inner-wrap -->
