<?php $title_for_layout = "Location de materiel";
$showSlider = false; 
?>

<h1><?php echo "Page Location"; ?></h1>
 <div id="body">
             	<div class="container demo-3">
             		<ul class="grid cs-style-4">
    
		<?php if ($loc_details == true ): ?>
				<li>
					<figure>
						<div><img src="../images/test_loc/moto_loc.jpg" alt="img05"></div>
						<figcaption>
							<h3><?php echo $locations->titre; ?></h3>
							<span><?php echo $locations->descriptif; ?></span>
						</figcaption>
					</figure>
				</li>
		<?php else: ?>
			<?php foreach ($locations as $v): ?>

				<!-- <li><a href="/location/<?php echo $v->id; ?>"><?php echo $v->titre; ?></a></li> -->


			<!-- Top Navigation -->
					
			
				<li>
					<figure>
						<div><img src="../images/test_loc/moto_loc.jpg" alt="img05"></div>
						<figcaption>
							<h3><?php echo $v->titre; ?></h3>
							<span><?php echo $v->descriptif; ?></span>
							<a href="<?php echo $v->getUrl(); ?>">Plus...</a>
						</figcaption>
					</figure>
				</li>
			<?php endforeach ?>
		<?php endif ?>
		
		
			</div>
 </div>