<div id="header-wrap" style="background-image:url('images/header-page-resa.png');min-height:250px;">
	<h2 id="title">Galerie photo</h2>				
</div>

<div id="main no-img-bg">		
	<div id="featdishes">
		<div class="header-menu">
			<div class="btn">
				<a href="/reservation">Reservez votre table</a>
			</div>
		</div>
	</div>

	<div class="gallery-content">
	<div id="magalerie">
		<?php foreach ($galerie as $image): ?>
			<a class="fancy" href="<?php echo $image['path']; ?> ">
				<img src="<?php echo $image['path']; ?>" alt="<?php echo $image['name']; ?> ">
			</a>
		<?php endforeach ?>
</div>
 </div>
<div class="cleaner">&nbsp;</div> 

	