<?php $showSlider = false; ?>
<?php $title_for_layout = "Espace sécurisé"; ?>

<div class="login_form">
	<h1>Espace sécurisé</h1>
	<form action="<?php echo Router::url('login'); ?>" method="post">
		<h3>Vous devez entrer vos identifiants de connexion pour consulter cet espace.</h3>
		<div class="error"><?php echo isset($errors)? $error:'' ?></div>
		<?php echo $this->form->input('email','Identifiant'); ?>
		<?php echo $this->form->input('password','Mot de passe',array( 'type' => 'password')); ?>
		<div class="Actions">
			<input type="submit" value ="Connexion">
		</div>
	</form>
</div>