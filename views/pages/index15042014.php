<?php $title_for_layout = "Restaurant Taxi-Brousse"; ?>
<div id="main">
  <div id="slider">
    <ul class="slides">
      <li style="background-image:url(../images/slider/IMG_0669.jpg)">&nbsp;</li>
      <li style="background-image:url(../images/slider/IMG_0670.jpg)">&nbsp;</li>
      <li style="background-image:url(../images/slider/IMG_0672.jpg)">&nbsp;</li>
      <li style="background-image:url(../images/slider/IMG_0677.jpg)">&nbsp;</li>
      <li style="background-image:url(../images/slider/IMG_0678.jpg)">&nbsp;</li>
      <li style="background-image:url(../images/slider/IMG_0720.jpg)">&nbsp;</li>
  </ul>
  <div class="details">
      <div>
        <ul class="items">
          <li>
            <span>
                <h3>Venez découvrir la cuisine africaine</h3>
                <div class="excerpt">Faites le tour culinaire de l'Afrique avec le Taxi-Brousse .
              </div>             
            </span>
          </li>
          <li>
            <span>
              <h3>Un cadre accueillant et chaleureux</h3>
              <div class="excerpt">Delectez vous de la cuisine et de l'ambiance chaleureuse dans notre restaurant [&hellip;]
              </div>              
            </span>
          </li>
          <li>
            <span>
              <h3>Ouvert tous les jours!</h3>
              <div class="excerpt">Wifi gratuit pour tous[&hellip;]
              </div>              
            </span>
          </li>
          <li>
            <span>
                <h3>Reservation possible par Internet ou sur notre site</h3>
                <div class="excerpt">Réservez directement en ligne [&hellip;]
                </div>
            </span>
          </li>
          <li>
            <span>
              <h3>Repas de groupes, anniversaires, fetes</h3>
              <div class="excerpt">Organisez des évènements spéciaux dans un cadre convivial et agréable.
              </div> 
            </span>
        </li>
        <li>
          <span>
              <h3>Celebrez vos occasions spéciales dans un cadre inoubliable!</h3>
              <div class="excerpt">Fetes de famille, anniversaires, banquets, rendez ces evenements inoubliables.!
              </div>
          </span>
        </li>
      </ul>
    </div>
  </div>
  <div id="slidenav">
    <ul>
      <li>
        <div class="post-thumb">
          <img src="images/thumbs/IMG_0669.jpg" alt="Cuisine africaine" class="Thumbnail thumbnail featured-thumb " width="142" height="96" /></div><h4>Venez découvrir la cuisine africaine</h4>
      </li>
      <li style="color:#000000;background-color:#f00452">
        <div class="post-thumb">
          <img src="images/thumbs/IMG_0670.jpg" alt="Cadre de reve" class="Thumbnail thumbnail featured-thumb " width="142" height="96" /></div><h4>Un cadre accueillant et chaleureux</h4>
      </li>
      <li style="color:#000000;background-color:#fa8f54">
        <div class="post-thumb"><img src="images/thumbs/IMG_0672.jpg" alt="Restaurant à Bordeaux" class="Thumbnail thumbnail featured-thumb " width="142" height="96" /></div><h4>Ouvert tous les jours!</h4>
      </li>
      <li style="color:#000000;background-color:#f5bb00">
        <div class="post-thumb"><img src="images/thumbs/IMG_0677.jpg" alt="Rservation sur le site" class="Thumbnail thumbnail featured-thumb " width="142" height="96" /></div><h4>Reservation ou commande par téléphone</h4>
      </li>
      <li style="color:#000000;background-color:#aed665">
        <div class="post-thumb"><img src="images/thumbs/IMG_0678.jpg" alt="Repas de groupe" class="Thumbnail thumbnail featured-thumb " width="142" height="96" /></div><h4>Repas de groupes</h4>
      </li>
      <li style="color:#ffffff;background-color:#5ea1d5">
        <div class="post-thumb"><img src="images/thumbs/IMG_0678.jpg" alt="Occasions spéciales!" class="Thumbnail thumbnail featured-thumb " width="142" height="96" /></div><h4>Celebrez vos occasions spéciales!</h4>
      </li>
    </ul>
  <div class="cleaner">&nbsp;</div>
</div>
</div>

<div id="content-wrap">

    <div id="btnbox">

        <h2>Bienvenue au restaurant Taxi-Brousse!</h2>

        <div class="content p-presentation">
          <h3>Un nouveau voyage culinaire commence à bord du Taxi-Brousse.</h3>
          <div class="threecol-one">
                <div class="post-thumb"><img src="images/thumbs/logo-tb.png" alt="Presentation 4" class="Thumbnail thumbnail featured-dishes " width="300" height="190" /></div>              

              Situé en <span class="texteenavant">plein coeur des 
              Capucins à Bordeaux</span>, le Taxi-Brousse vous initie et vous convie à un <span class="texteenavant">voyage gastronomique inédit</span> autour de l'Afrique.           
          </div>
          <div class="threecol-one">
            <div class="post-thumb"><img src="images/thumbs/ambiance-resto.jpg" alt="Presentation 4" class="Thumbnail thumbnail featured-dishes " width="300" height="190" /></div>              

              Le Restaurant vous accueille pour déguster une <span class="texteenavant">authentique cuisine africaine</span>, notamment nos spécialités végétariennes
              dans un <span class="texteenavant">cadre élégant</span> où règnent la <span class="texteenavant">joie et la bonne humeur</span>.
          </div>
          <div class="threecol-one last">
                  <div class="post-thumb"><img src="http://dummyimage.com/300x190/4d494d/686a82.gif&text=image+test" alt="Presentation 4" class="Thumbnail thumbnail featured-dishes " width="300" height="190" /></div>              

              En couple, en famille, entre amis ou collègues, venez <span class="texteenavant">déguster des plats typiquement africains et redécouvrir vos sens</span>, 
             le temps d'un voyage à travers l'Afrique profonde, du SENEGAL au CAMEROUN via le MALI ou la Cote d'Ivoire, le voyage etant assuré
              par le contenu de nos plats
          </div>
           <div class="cleaner">&nbsp;</div>
          <p class="texteenavant">BON APPETIT</p>
        </div>

          <div class="btn">
              <a href="">Réserver maintenant</a>
          </div>

      </div>
      <div id="about">

        <div class="hours">
          <h3>Horaires d'ouverture</h3>

          <div class="content">
            <p>Du Lundi au Samedi<br />
              10:00h à 00:00h <br />
              Dimanche<br />
              11:00h à 00:00h </p>
          </div>
      </div>

      <h2>A propos du Taxi-Brousse</h2>

      <div class="content">
          <p> Sur place ou à emporter, découvrez nos plats selon vos envies du moment.</p>
      </div>

      <div class="btn">
          <a href="<?php echo Router::url('menu'); ?> ">Consultez le menu</a>
      </div>

      <div class="cleaner">&nbsp;</div>

  </div>

  
  <div id="featured-dishes">

    <h2>Nos menus</h2>
    <ul>
      <li>
          <a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" rel="lightbox"  title="Apple Pie">
            <div class="post-thumb"><img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" alt="Mbongo Tchobi" class="Thumbnail thumbnail featured-dishes " width="220" height="150" /></div>             
        </a>

        <div class="content">
            <h3>Menu Midi</h3>
            <p>Entree + plat du jour  </p>
            <p>20€</p>
        </div>
    </li><li>

    <a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" rel="lightbox"  title="Poisson braisé">
        <div class="post-thumb"><img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" alt="Poisson braisé" class="Thumbnail thumbnail featured-dishes " width="220" height="150" /></div>              </a>

        <div class="content">
            <h3>Menu "LONDIE"</h3>
            <p>Menu midi<br>


            </p>

        </div>
    </li><li>

    <a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" rel="lightbox"  title="Presentation 3">
        <div class="post-thumb"><img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" alt="Recette 3" class="Thumbnail thumbnail featured-dishes " width="220" height="150" /></div>     
    </a>
    <div class="content">
        <h3>Menu "LAH GO"</h3>
          <p>Menu Végétarien.<br> 
            Samossa aux légumes.<br>
            Assiette de dégustation composée de 2 plats chauds:<br>
              <ul>
                <li>Yassa(Oignons citronnés au riz)</li>
                <li>Cassolette d'haricot rouge et ses alloco</li>
              </ul>
              Salade de fruits tropicaux ou glace
          </p>
    </div>
</li><li>

<a href="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" rel="lightbox"  title="Presentation 4">
    <div class="post-thumb"><img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=image+test" alt="Presentation 4" class="Thumbnail thumbnail featured-dishes " width="220" height="150" /></div>              
</a>

<div class="content">
    <h3>Menu LOMDIE</h3>
    <p>Degustation à partager pour 2<br>
        Cocktail
        Avec alcool : Punch d'amour
        Sans alcool : Bissap<br>
        TAPAS: Accras de Morue<br>
        Entree: Salade Taxi-Brousse<br>
        Plats:
        Assiette de mix Taxi Brousse(assortiment de viandes braisées(mouton,boeuf,poulet))
        ou
        Assiette de poisson braisé
        Dessert : Salade de fruits tropicaux ou Glaces 
      </p>
      <span class="formule-price">59€</span>
</div>
</li>
</ul>
<div class="cleaner">&nbsp;</div>
</div>



</div>

      </div><!-- / #main -->