<?php

	/**
	* 
	*/
	class CatMenu extends Model
	{
		public $table = "Cat_menus";

		
		public function __construct()
		{
			parent::__construct();
		}


		public function getAllCatMenus()
		{
			$sql = "SELECT id,libelle FROM $this->table";
			$pre = $this->db->prepare($sql);
			$pre->execute();
			$arr = $pre->fetchAll( PDO::FETCH_OBJ );
			return $arr;
		}


	}
?>