<?php 
	require('CatMenu.php');
	/**
	* 
	*/
	class Carte extends Model
	{
		
		public $table = "menus";
		
		public function __construct()
		{
			parent::__construct();
		}

		public function getMenus()
		{

		}

		//Récupère la liste des menus et les range par catégorie
		public function getAllMenus()
		{

		}

		//Recupere la liste des formules 
		public function getFormules()
		{

		}

		public function save( $data )
		{
			$date = new DateTime('now');
			if ( $data->id ) {
				$object->id = $data->id;
				$object->datemodif = $date->format("Y/m/d H:i:s");
			} else{
				$object->datecreation = $date->format("Y/m/d H:i:s");
				$object->datemodif = $date->format("Y/m/d H:i:s");
			}
			$object->categorie = $data->categorie;
			$object->type_id = $data->type_id;
			$object->titre = $data->titre;
			$object->images = serialize( $data->images_urls );
			$object->slug = create_slug( $data->titre );
			$object->description = $data->description;
			return parent::save($object);
		}




		private function saveMediaContact( $data,$files )
		{
			$handle = new upload($files['files'],'fr_FR');
			$handle->auto_create_dir = true;
			$handle->allowed = array('application/pdf','application/msword', 'image/*');
			$fileDest = Conf::$imagesConf['docs_user'];
			$handle->file_new_name_body = $data->nom.'-'.$data->societe.' '.$files['name'];
        	$handle->process($fileDest);

        	if ( $handle->processed ) {
        		// fichier créé et uploadé, on cree notre objet image qui sera leur référence
        		$media = new Media();
        		$object->url = $handle->file_dst_path;
        		$object->titre = $handle->file_dst_name;
	        	return $media->save($object);
        	} else{
        		$this->Session->SetFlash('Erreur lors de la sauvegarde du fichier. Merci de reessayer');
        		return false;
        	}

		}

		public function saveContact( $data,$files )
		{
			//On sauvegarde l'image et passe les id images dans l'objet
			$media_id =  $this->saveMediaContact($data,$files );
			$date = new DateTime('now');
			$object->email = $data->email;
			$object->password = $data->password;
			$object->nom = $data->nom;
			$object->prenom = $data->prenom;
			$object->societe = $data->societe;
			$object->telephone = $data->telephone;
			$object->usertype_id = 3;
			$object->datecreation = $date->format('Y-m-d H:i:s');
			$object->datemodif = $date->format('Y-m-d H:i:s');
			$object->media_id = $media_id;
			return parent::save( $object);
			// Apres sauvegarde, on essaie de recuperer l'url d'inscription
		}

		//Recupere le liste des documents d'un contact
		public function getDocContact( $id )
		{
			$sql = "SELECT url FROM media WHERE id=(SELECT media_id FROM USERS where id=".$id;
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			return $stmt->fetch();
		}

	}


?>