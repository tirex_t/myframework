<?php

	/**
	*   Classe de reservation de table ou de menu.
	*/
	class Intervenants extends Model
	{
		public $table = "intervenants";
		public $data;

		public $validate = array(
			'nom' => array(
					'rule' => 'notEmpty',
					'message'=>'Le nom ne peut pas etre vide!'
				),
			'date' => array(
					'rule' => 'datetime',
					'message' => "La date doit etre valide"
				),
			'heure' => array(
					'rule' => 'hour',
					'message' => 'Heure incorrecte'
				)
		);

		
		public function __construct()
		{
			parent::__construct();
		}

		public function setData( $data )
		{
			$this->data = $data;
		}

		public function getUrl()
		{

		}


		public function save( $data ){
			$date = new DateTime('now');
			if ( $data->id ) {
				//Maj de l'objet
				$object->id = $data->id;
				$object->datemodif = $date->format("Y/m/d H:i:s");
			} else {
				$object->datecreation = $date->format("Y/m/d H:i:s");
				$object->datemodif = $date->format("Y/m/d H:i:s");
			}
			
			$object->nom = $data->nomresa;
			$object->prenom = $data->prenomresa;
			$object->email = $data->emailresa;
			$object->dateresa = $data->dateresa ;
			$object->heureresa = $data->heureresa ;
			$object->nbrecouverts =  $data->couvertsresa ;
			$object->messageresa = $data->messageresa;
			parent::save( $object );

		}
	}
?>