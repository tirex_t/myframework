<?php

	/**
	* 
	*/
	class Users extends Model
	{
		public $table = "user";
		
		public function __construct()
		{
			parent::__construct();
		}


		public function connect($username,$password)
		{
			$sql = "SELECT * FROM ".$this->table." where username = :user and password = :pass";
			$stmt = $this->db->prepare($sql);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Users");
			$stmt->execute(array(
				":user" => $username,
				":pass" => $password
			));
			
			return $stmt->fetch();
		}


		public function getDroits( $user_id )
		{
			// $req = "SELECT * FROM user_droit WHERE id_user = :id ";
			// $stmt = $this->db->prepare($req);
			// $stmt->setFetchMode(PDO::FETCH_ASSOC);
			// $stmt->execute(array(
			// 	":id" => $user_id
			// ));
			// $droits = $stmt->fetch();
			return $this->usertype_id;			
			// return $droits['id_droit'];
		}


		public function beforeFirstSave( $object )
		{
			$date = new DateTime('now');
			$object->datecreation  = $date->format("d/m/Y H:i:s ");
			return $object;
		}


		public function save( $object )
		{
			if ( !$object->id ) {

				$object = $this->beforeFirstSave( $object );	
			} else {
				$object = $this->beforeSave( $object );
			}
			$key = $object->id;
			$champs=array();
			$d = array();
			$action =''; //L'action qui a été faite: Update ou Insert
			// if(isset($object->id)) unset($object->id);

			foreach ( $object as $k => $v ) {
				$champs[] = "$k=:$k";
				$d[":$k"] = $v;
			}
			//On va verifier tous les éléments faisant partie de l'objet pour sauvegarde
			if (isset($key) && !empty($key)) {
				//Update
				$sql = 'UPDATE '.$this->table.' SET '.implode(',', $champs).' WHERE id=:'.$key;
				$this->id = $object->id;
				$action ='update';
			} else{

				$sql = 'INSERT INTO '.$this->table.' SET '.implode(',', $champs);
				$action = 'insert';
			}

			$pre = $this->db->prepare($sql);
			$pre->execute($d);

			if ( $action =='insert') {
				$this->id = $this->db->lastInsertId();	
			}
			return true;
		}

		public function beforeSave($object)
		{
			return $object;
		}


		public function saveLog($data)
		{
			$date = new DateTime('now');
			$data['datecreated'] = $date->format('Y-m-d H:i:s');
			$champs = array();
			$d = array();
			foreach ( $data as $k => $v ) {
				$champs[] = "$k=:$k";
				$d[":$k"] = $v;
			}

			$sql = 'INSERT INTO user_log SET '.implode(',', $champs);
			$pre = $this->db->prepare($sql);
			$pre->execute($d);
			return true;
		}
	}
?>