<?php

	/**
	* 
	*/
	class Media extends Model
	{

		private $confFile;
		public $table = "Media";

		public $id;
		private $url;
		private $thumbUrl;
		private $titre;
		private $description;
		private $datecreated;
		private $datemodif;


		public function __construct( $url,$thumb,$titre,$description )
		{
			parent::__construct();
			$this->url = $url;
			$this->thumbUrl = $thumb;
			$this->titre = $titre;
			$this->description = $description;
		}


		public function getUrl()
		{
			return $this->url;
		}

		public function setUrl( $url )
		{
			$this->url = $url;
		}

		public function getTitre()
		{
			return $this->titre;
		}

		public function getThumbUrl()
		{
			return $this->thumbUrl;
		}

		public function setThumbUrl( $url )
		{
			return $this->thumbUrl;
		}

		public function getDescription()
		{
			return $this->description;
		}

		public function setDescription($desc)
		{
			$this->description = $desc;
		}


		private function checkMimetype( $type ){
		    $typesacceptes = array('image/jpeg','image/jpg','image/png','image/gif','image/pjpeg');
		    if(in_array($type,$typesacceptes)) return true;
				else return false;
		}

		public function getGallery()
		{
			//Charge toutes les images depuis le dossier Galerie
			$folder = Conf::$imagesConf['gallery_path'];

			$sortedArray = array();
			foreach(array_diff(scandir($folder), array('.','..')) as $file) {
				$arr = array();
				$arr['name'] = $file;	
				$arr['path'] = $folder.'/'.$file;
				$sortedArray[] = $arr;
			}
			return $sortedArray;
		}

		private function uploadZik( $file )
		{
			$extension = end(explode('.', $_FILES['image']['name']));
			if ($extension!='mp3') {
				
			}
			//Upload un fichier de musique et retourne le nom du fichier uploadé
			$handle = new upload($file['file'],'fr_FR');
			$handle->auto_create_dir = true;
			$handle->allowed = array('*');
			$fileDest = Conf::$imagesConf['music'];

        	$handle->process($fileDest);

        	if ( !$handle->processed ) {
        		$this->Session->SetFlash('Erreur lors de la sauvegarde du fichier:'.$handle->error.' Merci de reessayer');
        	} 
		
		}

		public function getAllMusic()
		{
			//Charge toutes les images depuis le dossier Galerie
			$folder = Conf::$imagesConf['music'];

			$sortedArray = array();
			foreach(array_diff(scandir($folder), array('.','..')) as $file) {
				$arr = array();
				$arr['name'] = $file;	
				$arr['path'] = $folder.'/'.$file;
				$sortedArray[] = $arr;
			}
			return $sortedArray;	
		}


		public function getAllMedias()
		{

			$cond = array(
					'champs' => 'id,titre,description,url,thumb_url,datemodif'
				);
			return $this->find($cond);
		}

		public function save()
		{
			$date = new DateTime('now');
			$this->datecreated = $date->format("Y/m/d H:i:s ");
			$this->datemodif = $date->format("Y/m/d H:i:s ");
			$object->url = PROJECT_URL.str_replace("\\","/",$this->url); 
			$object->thumb_url = PROJECT_URL.str_replace("\\","/",$this->thumbUrl);
			$object->titre = $this->titre;
			$object->description = $this->description;
			$object->datecreated = $this->datecreated;
			$object->datemodif = $this->datemodif;
			parent::save($object);
		}
 
	}
?>