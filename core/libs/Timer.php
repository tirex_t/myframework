<?php
    
    /**
     *  Classe qui permet de calculer le temps ecoulé entre 2 evenements.
     * Framework Citrus
     */
    
    class Timer {
        private $startTime;
        private $endTime;
        public $execTime;
        public $label;
        
        public function __construct( $label ) {
            $this->label = $label;
            $this->startTime = microtime( true );
        }
        
        public function start() {
            $this->startTime = microtime( true );
        }
        
        public function stop() {
            $this->endTime = microtime( true );
            $this->execTime = $this->endTime - $this->startTime;
        }
        
        public function getExecTime() {
            return $this->format_date($this->execTime);
            // return round( $this->execTime * 1000, 3 );
        }
    
        private function format_date( $secondes )
        {
          // $hours = (int)($minutes = (int)($seconds = (int)($milliseconds = (int)($secondes * 1000)) / 1000) / 60) / 60;
          // return $hours.'h:'.($minutes%60).':min'.($seconds%60).(($milliseconds===0)?'sec,':'.sec,'.rtrim($milliseconds%1000, '0'));
            // extract hours
            $hours = floor($secondes / (60 * 60));
         
            $divisor_for_minutes = $secondes % (60 * 60);
            $minutes = floor($divisor_for_minutes / 60);
         
            $divisor_for_seconds = $divisor_for_minutes % 60;
            $seconds = ceil($divisor_for_seconds);
            
            $obj = $hours.'h:'.$minutes.'min:'.$seconds.'sec';
            // return the final array
            // $obj = array(
            //     "h" => (int) $hours,
            //     "m" => (int) $minutes,
            //     "s" => (int) $seconds,
            // );
            return $obj;
        }
}
?>