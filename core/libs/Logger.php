<?php
class Logger {
    private $_dir = '';
    private $_file;
    private $_fileName;
    private $_linePrefix = '';
    public $events;

    public function __construct( $name ) {
        $this->_linePrefix = date( '[Y-m-d H:i:s]' ) . " ";
        $this->_fileName = APP_LOG_PATH . $name . '-' . date( 'Ym') . '.log';
    }
    
    public function logEvent( $str ) {
        $this->events[] = $this->_linePrefix . $str;
    }
    
    
}
?>