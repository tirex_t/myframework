<?php

class File {
    public $resource;
    public $mode;
    
    const MODE_ROS = 'r';
    const MODE_ROE = 'r+';
    const MODE_WOS = 'w';
    const MODE_RWS = 'w+';
    const MODE_WOE = 'a';
    const MODE_RWE = 'a+';
    
    public function __construct( $resource, $mode ) {
        $this->resource = $this->open( $resource, $mode );
    }
    
    public function open( $resource, $mode ) {
        return fopen( $resource, $mode );
    }
    public function write( $str ) {
        if ( $this->resource ) {
            return fwrite( $this->resource, $str );
        } return false;
    }
    
    public function close() {
        if ( $this->resource ) {
            fclose( $this->resource );
        }
    }
}
?>