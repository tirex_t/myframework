<?php

abstract class Model
{
	public $conf = 'default';
	static $connections = array();
	public $table = false;
	public $db;
	public $id;
	public $lastInsertid;
	protected $validateArr = array(); //validation des formulaires, à redefinir dans chaque Modele si besoin
	public $errors = array();
	public $form; //Contient le formulaire générique du modèle

	public function __construct()
	{	
		//On determine le nom de la table à partir du modele appelant
		if ($this->table === false) {
			$this->table = strtolower(get_class( $this)); 
		}
		if (isset(Model::$connections[$this->conf])) {
			$this->db = Model::$connections[$this->conf];
			return true;
		}

		$conf = Conf::$databases[$this->conf];
		try {
			$pdo = new PDO(
					'mysql:host='.$conf['host'].';dbname='.$conf['database'].';', 
					$conf['user'],
					$conf['passwd'],
					array(
						PDO::MYSQL_ATTR_INIT_COMMAND =>'SET NAMES utf8'
					)
				);
			$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			Model::$connections[$this->conf] = $pdo;
			$this->db = $pdo;

		} catch (PDOException $e) {
			if (Conf::$debug >= 1 ) {
				die($e->getMessage());
			} else {
				die("Impossible de se connecter à la base");
			}

		}
		
	}


	public function findAll()
	{
		$sql = 'SELECT * FROM '.$this->table.' as '.get_class($this).'';
		$pre = $this->db->prepare($sql);
		try {
			$pre->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
		return $pre->fetchAll(PDO::FETCH_CLASS,get_class($this));
	}

	public function findOne( $id )
	{

		$sql = "SELECT * FROM ".$this->table.' as '.get_class($this).' WHERE id='.$id;
		$pre = $this->db->prepare($sql);		
		$pre->execute();
		// return $pre->fetch(PDO::Obj, get_class($this));
		return $pre->fetch(PDO::FETCH_OBJ);
	}

	public function find($req)
	{
		// $sql = 'SELECT * FROM'.$this->table.' as '.get_class($this).'';
		$sql = "SELECT ";
		
		if (isset($req['champs'])) {
			if (!is_array($req['champs'])) {
				$sql.=	$req['champs'];
			} else {
				$sql.= implode(",", $req['champs']);
			}
		} else{
			$sql.= "*";
		}
		$sql .= ' FROM '.$this->table.' as '.get_class($this).'';

		//construction des conditions
		if (isset($req['conditions'])) {
			$sql.=" WHERE ";
			if (!is_array($req['conditions'])) {
				//Ce n'est pas un tableau, je rajoute tout simplement ma phrase condition
				$sql.= $req['conditions'];
			} else {
				$cond = array();
				foreach ( $req['conditions'] as $k=>$v ) {
					//Eviter les injections sql
					if (!is_numeric($v)) {
						$v = '"'.mysql_real_escape_string($v).'"';
					}
					$cond[] = "$k = $v";
				}

				$sql.= implode(" AND ", $cond);
			}
		}

		if (isset($req['limit'])) {
			$sql.=" LIMIT ".$req['limit'];

		}
		$pre = $this->db->prepare($sql);
		$pre->execute();
		return $pre->fetchAll(PDO::FETCH_CLASS,get_class($this));
	}


	public function findFirst($req)
	{
		return current($this->find($req));
	}


	public function findCount($conditions)
	{
		$res = $this->findFirst(array(
			'champs' => 'COUNT(id) as count',
			'conditions' =>$conditions
		));

		return $res->count;
	}


	public function save($object)
	{
		$key = $object->id;
		$champs=array();
		$d = array();
		$action =''; //L'action qui a été faite: Update ou Insert
		// if(isset($object->id)) unset($object->id);

		foreach ( $object as $k => $v ) {
			$champs[] = "$k=:$k";
			$d[":$k"] = $v;
		}

		if (isset($key) && !empty($key)) {
			//Update
			$sql = 'UPDATE '.$this->table.' SET '.implode(',', $champs).' WHERE id='.$key;
			$this->id = $object->id;
			$action ='update';
		} else{
			$sql = 'INSERT INTO '.$this->table.' SET '.implode(',', $champs);
			$action = 'insert';
		}
		$pre = $this->db->prepare($sql);
		$pre->execute($d);

		if ( $action =='insert') {
			$this->lastInsertid = $this->db->lastInsertId();
		}
		return $this->db->lastInsertId();
	}



	public function delete($id)
	{
		$sql = 'DELETE * FROM '.$this->table.' Where id='.$id;
		$this->db->query($sql);
	}

	/**
	 * Valide les champs de saisie d'un formulaire
	 */
	public function validates( $data )
	{
		$errors = array();
		if ( count( $this->validateArr )) {
			foreach ($this->validateArr as $k => $v ) {
				if (!isset($data->$k)) { //Si le champ n'est pas dans data
					$errors[$k] = $v['message'];
				} else {
					if ($v['rule'] == 'notEmpty')  {
						if (empty($data->$k)) {
							$errors[$k] = $v['message'];	
						}
					} elseif (!preg_match('/'.$v['rule'].'$v', $data->$k )) {
						$errors[$k] = $v['message'];
					} //On peut rajouter ici d'autres tests de validation: date, valeur numerique, etc. 
					
				}
			}
			$this->errors = $errors;
			if (isset($this->Form)) {
				$this->Form->errors = $errors;
			}
			if (empty($errors)) {
				return true;
			} else{
				return false;
			}
		} else {
			return true;
		}
	}

}
?>