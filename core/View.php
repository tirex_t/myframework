<?php 

	class View
	{
		/**
     	* Nom de la vue
     	* @var string Vue
    	*/
	    protected $file;

    	/**
     	* Global data
     	* @var array Data
     	*/
     	protected $data = array();

     	public $rendered = false;
     	public $form;

     	/**
     	 *  Parties à inclure à la vue, tbd
     	 */
     	private $partials = array();
     	public $request;
     	public $layout 		= 'default';
     	public $route;
     	public $session;
    	

     	public function __construct($request,$form,$session)
     	{
     		$this->request = $request;
     		$this->form = $form;
     		$this->session = $session;
     	}

     	/**
     	 * On verifie dans le tableau params si des partials sont demandés et on les rajoute.Idem pour le js
     	 */

		public function render($view,$params = null)
		{

			if($this->rendered) return false;
			
			if (strpos($view, '/')) {
				$view = ROOT.'view'.$view.'.php';
			}else $view= ROOT.DS.'views'.DS.$this->request->controller.DS.$view.'.php';
			//On verifie le prefix et on choisit la bonne vue en fonction de lui
			extract( $params['vars'], EXTR_OVERWRITE );

			if ( $this->request->prefix ) {
				$this->layout = $this->request->prefix;
			}

			ob_start();
			require $view;
			$content = ob_get_clean();
			require ROOT.DS.'views'.DS.'layout'.DS.$this->layout.'.php';

			$this->rendered = true;
		}



		/**
		 *  Ajoute un partial avec ses params au tableau des partials
		 */
		public function addPartial( $partial,$params )
		{
			$this->partials['name'] = $partial;
			$this->partials['file'] = ROOT.DS.'views'.DS. 'layout'.DS.$partial.'.php';
			$this->partials['params'] = $params;
		}

		public function renderAjax( $data )
		{
			if ( $data ) {
				echo json_encode($data);
				exit;
			}
		}

		/**
		 *  Ajoute une feuille js à la vue demandée
		 */
		public function addJsFile($filename)
		{
			return "";
		}
		/**
		 *  Ajoute une feuille js à la vue demandée
		 */
		public function addCssFile($filename,$default = 'default')
		{
			if ( $default == 'admin') {
				$cssfolder = PROJECT_URL.'admin'.DS.'css';
			} else {
				$cssfolder = PROJECT_URL.'css'.DS;
			}
			 $link = '<link rel="stylesheet" type="text/css" href="'.$cssfolder.$filename.'.css" media="screen" />';
			return $link;
		}

		public function writeUrl( $url )
		{
			
		}
	}

 ?>
