<?php
	require "libs/Timer.php";
	require "libs/class.phpmailer.php";
	require "Session.php";
	require "Request.php";
	require "Router.php";
	require ROOT.DS.'config'.DS.'conf.php';
	require ROOT.DS.'config'.DS.'routes.php';
	require "Form.php";
	if (Conf::$debug>=1) {
		require "phpbuglost_lite.php";
	}
	require "Model.php";
	require "View.php";
	require "libs/class.upload.php";
	require "Controller.php";
	require "Dispatcher.php";
?>