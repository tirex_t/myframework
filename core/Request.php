<?php
	
	/**
	* 
	*/
	class Request
	{
		/**
		 * Url appelée par l'utilisateur
		 */
		public $url;
		public $page = 1;
		public $prefix = false;
		public $data = false;
		public $isXHR;
		
		function __construct()
		{
			$this->url = isset($_SERVER['PATH_INFO'])? $_SERVER['PATH_INFO']:'/' ;
	        $this->isXHR = isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

			if (isset($_GET['page'])) {
				if (is_numeric($_GET['page'])) {
					if ($_GET['page']>0) {
						$this->page = round($_GET['page']);	
					}
					
				}
			}
			//Traitement du formulaire rempli, on remplit le request data avec toutes les données soumises dans le formulaire
			if (!empty($_POST)) {
				$this->data = new stdClass();
				foreach ($_POST as $k => $v) {
					$this->data->$k = $v;
				}
			}
		}
	}
	
?>