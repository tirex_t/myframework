<?php 
	function vexp($var,$pre = true)
	{
		$st ='';
		if($pre) $st.='<pre>';
		$st.= var_export($var,true);
		if($pre) $st.='</pre>';
		echo $st;
		exit;
	}


	function debug($var)
	{
		$back = debug_backtrace();
		echo '<p>&nbsp;</p><p><a href="#" onclick="$(this).parent().next(\' ol \').slideToggle(); return false;"><strong>'.$back[0]['file'].'</strong> Ligne: '.$back[0]['line'].'</a></p>';
		echo '<ol style="display:none;">';
		foreach ($back as $k => $v) {
			if ($k>0) {
				echo '<li><strong>'.$v['file'].'</strong> Ligne: '.$v['line'].'</li>';
			}
		}
		echo '</ol>';
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}

	function file_size($size)
	{
    	$filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    	return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
	}

	function getFilesize( $val )
	{
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);

		switch($last)
				{
					case 'g':  $val *= 1024;
					case 'm': $val *= 1024;
					case 'k':  $val *= 1024;
				}

		return $val;
	}


	function generatePassword ($length = 8)
	{
	  $password = "";
	  $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
	  $maxlength = strlen($possible);
	  if ($length > $maxlength) $length = $maxlength;
	  $i = 0; 
	  while ($i < $length) { 
	    $char = substr($possible, mt_rand(0, $maxlength-1), 1);
	    if (!strstr($password, $char)) { 
	      $password .= $char;
	      $i++;
	    }
	  }
	  return $password;
	}

	function isInteger( $int ) {
	    return is_int( intval( $int ) );
	}

	function isMobile() {
	    $keys = array(
	        'alcatel', 'amoi', 'android', 'avantgo', 'blackberry', 'benq', 
	        'cell', 'cricket', 'docomo', 'elaine', 'htc', 'iemobile', 'iphone', 
	        /*'ipad',*/ 'ipaq', 'ipod', 'j2me', 'java', 'midp', 'mini', 'mmp', /*'mobi', */
	        'motorola', 'nec-', 'nokia', 'palm', 'panasonic', 'philips', 'phone', 
	        'sagem', 'sharp', 'sie-', 'smartphone', 'sony', 'symbian', 't-mobile',
	        'telus', 'up\.browser', 'up\.link', 'vodafone', 'wap', 'webos', 'wireless', 
	        'xda', 'xoom', 'zte'
	    );
	    $exp = implode( '|', $keys );
	    return preg_match( '/(' . $exp . ')/i', $_SERVER['HTTP_USER_AGENT'] );
	}

	function reduireChaine($chaine, $nb_car, $delim='...') {
	  $length = $nb_car;
		if($nb_car<strlen($chaine)){
			 while (($chaine{$length} != " ") && ($length > 0)) {
	   			$length--;
	  	}
	  	if ($length == 0) return substr($chaine, 0, $nb_car) . $delim;
	   		else return substr($chaine, 0, $length) . $delim;
	  	}else return $chaine;
	}


	
	function create_slug($str, $options = array())
	{
	// Make sure string is in UTF-8 and strip invalid UTF-8 characters
		$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
		$defaults = array(
			'delimiter' => '-',
			'limit' => 200,
			'lowercase' => true,
			'replacements' => array(),
			'transliterate' => false,
			);
	// Merge options
		$options = array_merge($defaults, $options);
		$char_map = array(
	// Latin
			'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
			'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
			'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
			'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
			'ß' => 'ss',
			'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
			'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
			'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
			'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
			'ÿ' => 'y',

	// Latin symbols
			'©' => '(c)',

	// Greek
			'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
			'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
			'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
			'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
			'Ϋ' => 'Y',
			'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
			'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
			'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
			'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
			'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',

	// Turkish
			'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
			'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',

	// Russian
			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
			'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
			'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
			'Я' => 'Ya',
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
			'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
			'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
			'я' => 'ya',

	// Ukrainian
			'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
			'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',

	// Czech
			'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
			'Ž' => 'Z',
			'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
			'ž' => 'z',

	// Polish
			'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
			'Ż' => 'Z',
			'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
			'ż' => 'z',

	// Latvian
			'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
			'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
			'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
			'š' => 's', 'ū' => 'u', 'ž' => 'z'
			);
	// Make custom replacements
			$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
	// Transliterate characters to ASCII
			if ($options['transliterate']) {
			$str = str_replace(array_keys($char_map), $char_map, $str);
		}
		// Replace non-alphanumeric characters with our delimiter
		$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
		// Remove duplicate delimiters
		$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
		// Truncate slug to max. characters
		$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
		// Remove delimiter from ends
		$str = trim($str, $options['delimiter']);
		return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
	}

	function format_date( $secondes )
	{
	  $hours = (int)($minutes = (int)($seconds = (int)($milliseconds = (int)($secondes * 1000)) / 1000) / 60) / 60;
	  return $hours.':h'.($minutes%60).':min'.($seconds%60).(($milliseconds===0)?'sec,':'.sec,'.rtrim($milliseconds%1000, '0'));
	}


	function frDate( $date,$lang="fr" )
	{
	    $date_formatee = "";
	    $format = "%A %d %B %Y, %H:%M";

	    if ($lang == 'fr') setlocale(LC_TIME, "fr_FR");

	    $date_strtotime = strtotime($date);
	    $date_formatee = strftime ($format,$date_strtotime);
	    return $date_formatee;		
	}

	function fr_date($date)
	{
		if(intval($date) == 0) return $date;

		$tampon = time();
		$diff = $tampon - $date;

		$dateDay = date('d', $date);
		$tamponDay = date('d', $tampon);
		$diffDay = $tamponDay - $dateDay;

		if($diff < 60 && $diffDay == 0)
		{
			return 'Il y &agrave; '.$diff.'s';
		}

		else if($diff < 600 && $diffDay == 0)
		{
			return 'Il y &agrave; '.floor($diff/60).'m et '.floor($diff%60).'s';
		}

		else if($diff < 3600 && $diffDay == 0)
		{
			return 'Il y &agrave; '.floor($diff/60).'m';
		}

		else if($diff < 7200 && $diffDay == 0)
		{
			return 'Il y &agrave; '.floor($diff/3600).'h et '.floor(($diff%3600)/60).'m';
		}

		else if($diff < 24*3600 && $diffDay == 0)
		{
			return 'Aujourd\'hui &agrave; '.date('H\hi', $date);
		}

		else if($diff < 48*3600 && $diffDay == 1)
		{
			return 'Hier &agrave; '.date('H\hi', $date);
		}

		else
		{
			return 'Le '.date('d/m/Y', $date).' &agrave; '.date('h\hi', $date).'.';
		}
	}

?>