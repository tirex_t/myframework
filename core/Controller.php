<?php
	class Controller{
		
		/**
		 * 
		 */
		public $request;
		public $vars = array();
		protected $rendered = false;
		protected $view;
		protected $parpage;

		public function __construct( $request = null )
		{
			$this->Session = new Session();
			$this->Form = new Form($this);
			if ($request) {
				$this->request = $request;
				$this->set('debug', Conf::$debug);	
			}
			$this->parpage = Conf::$parPage;
			$this->view = new View($this->request,$this->Form,$this->Session);
			require ROOT.DS.'config'.DS.'hook.php';
		}

		public function render( $viewName,$params = null )
		{
			$this->beforeRender();
			$params['vars'] = $this->vars; //J'ajoute les paramètres au tableau des parametres avant de les passer à la vue
			$this->view = new View($this->request,$this->Form,$this->Session);
			$this->view->render($viewName,$params);
			exit;
		}

		public function renderError(  )
		{
			if($this->rendered) return false;
			extract($this->vars);
			$view= ROOT.DS.'views'.DS.'errors/404.php';
			ob_start();
			require $view;
			$content = ob_get_clean();
			require ROOT.DS.'views'.DS.'layout'.DS.$this->layout.'.php';
			$this->rendered = true;
		}

		public function e404($message)
		{
			extract($this->vars);
			$this->renderError();
		}


		public function set($key,$value = null)
		{
			if (is_array($key)) {
				$this->vars += $key;
			}else $this->vars[$key] = $value;
		}

		/**
		 * Ajoute un partial aux vars à passer à la vue
		 */
		public function setPartial($key,$value = null)
		{
			$partial = array();
			$partial['template'] = $key;
			if ($value) {
			 	if ( is_array( $value ) ) {
			 		//Si c'est un tableau on rajoute les parametres dans le tableau params du partial
			 		foreach ( $value as $k => $v ) {
			 			$partial[$k] = $v;
			 		}
			 	} 
			 } 
			$this->vars['partials'] = $partial;
		}

		/**
		 * Permet de charger un modèle
		 */
		public function loadModel($name)
		{
			$file = ROOT.DS.'models'.DS.$name.'.php';
			require_once($file);
			if (!isset($this->$name)) {
				$this->$name = new $name();	
				if (isset($this->Form)) {
					$this->$name->form = $this->Form;
				}
			}
		}

		/**
		 * Permet d'appeler un controller depuis une vue
		 */
		public function request($controller,$action)
		{
			$controller.='Controller';
			require_once ROOT.DS.'controllers'.DS.$controller.'.php';
			$c = new $controller();
		}		

		/**
		  *  Fonction redirect
		  */ 

		public function redirect($url,$code = null)
		{
			if ($code == 301) {
				header("HTTP/1.1 301 Moved Permanently");
			}
			header("Location: ".Router::url($url));
		}
		/**
		 * Charge un template et envoie un mail.
		 * params: @template: le template à charger
		 *		   @data: les parametres à passer à la vue
		 *          
		 */
		public function sendMail($type, $objet, $user )
		{
			$file ='';
			$mail = new phpmailer();
			$mail->FromName = $user->nom;
			$mail->From = $user->email;
			$mail->isHTML(true);
			$mail->Subject = $objet;

			switch ( $type ) {
				case 'inscription':
					$template = ROOT.DS.'views'.DS.'templates'.DS.'inscription'.'.tpl.php';
					break;
				case 'admin-inscription':
					$template = ROOT.DS.'views'.DS.'templates'.DS.'adminInscription'.'.tpl.php';
					$this->loadModel('users');
					$user->file = $this->Users->getDocContact($user->id); //Recupère l'url du fichier à ajouter pour l'admin
					$mail->addAddress(Conf::$mailConf['adminMail']);
					$mail->AddAttachment($user->file);
					break;
				case 'validation':
					$template = ROOT.DS.'views'.DS.'templates'.DS.'validation'.'.tpl.php';
					break;
				default:
					# code...
					break;
			}
			extract($user);
			// $view = ROOT.DS.'views'.DS.'templates'.DS.$template.'.tpl.php';
			ob_start();
			require $template;
			$content = ob_get_clean();
			
			$mail->Body    = $content;
			vexp($mail,true);exit;
			$resp = '';
			if(!$mail->Send()){ //Teste si le return code est ok.
		      $resp = $mail->ErrorInfo; //Affiche le message d'erreur
		    }
		    else{	  
		      $resp = 'Mail envoyé !';
		 
		    }
		    $mail->SmtpClose();
		    unset($mail); 
		    return $resp;
		}

		public function beforeRender()
		{
			
		}

	}
?>