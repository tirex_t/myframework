<?php

	class Mail
	{
		private $template = null;
    	private $path = null;
    	private $subject;
    	private $mail;


    	/**
    	 * Classe permettant d'envoyer des emails à partir de phpMailer
    	 */
    	public function __construct( $type,$params )
    	{
    		switch ($type) {
    			case 'inscription':
    				$this->template = VIEW_PATH.DS.'templates'.DS.'inscription.tpl.php';
    				$this->subject = " Inscription sur le site emmoo";
    				break;
    			case 'validation':
    				$this->template = VIEW_PATH.DS.'templates'.DS.'validation.tpl.php';
    				$this->subject = " Validation de votre inscription sur le site emmoo";
    				break;
    			case 'desinscription':
    				$this->template = VIEW_PATH.DS.'templates'.DS.'desinscription.tpl.php';
    				$this->subject = " Désinscription sur le site emmoo";
    				break;
    			case 'envoipasswd':
    				$this->template = VIEW_PATH.DS.'templates'.DS.'envoipasswd.tpl.php';
    				$this->subject = " Réinitialisation de votre mot de passe";
    				break;
    		}

    		$conf = Conf::$mailConf;
    		$this->mail = new \PHPMailer();
    		$this->mail->AddEmbeddedImage( IMAGE_PATH . '/web/images/logo.png', 'logo' );
    		$this->mail->IsHtml( true );
    		$this->mail->From = $conf['FromMail'];
    		$this->mail->FromName = utf8_decode($conf['FromName']);
    		$this->mail->Subject = $this->subject;
    		


    	}


    	public function mail( $to ) {
	    
	        $this->mail = new \PHPMailer();
	        $this->mail->AddEmbeddedImage( IMAGE_PATH . '/web/images/logo.png', 'logo' );
			$this->mail->IsHtml( true );
			$this->mail->From = Conf::$mailConf;
			$this->mail->FromName = utf8_decode("Nom emetteur");
			$this->mail->Subject = utf8_decode(  );
			$this->mail->Body = utf8_decode( );
			$this->mail->addAddress( $to );
			$this->mail->send();
	    }
	}
?>