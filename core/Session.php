<?php
	
	class Session
	{
		public function __construct()
		{
			if (!isset($_SESSION)) {
				session_start();
			}
		}

		/**
		 * Enregistre un message dans la session
		 */
		public function setFlash($message,$type = 'success')
		{
			$_SESSION['flash'] = 	array(
					'message' => $message,
					'type'    => $type
				);
		}

		public function hasFlash()
		{
			return isset($_SESSION['flash']);
		}

		public function flash()
		{
			if (isset($_SESSION['flash'])) {
				$arr = $_SESSION['flash']['message'];
				$message ='';
				if ( is_array( $arr )) {
					$message.="<ol>";
					foreach ( $arr as $mess ) {
						$message.= "<li>".$mess."</li>";
					}
					$message.="</ol>";
				} else {
					$message = $arr;
				}
				$html =  '<div class="header-information alert alert-'.$_SESSION['flash']['type'].'">'.$message.'</div>';
				$_SESSION['flash'] = array();
				// unset( $_SESSION['flash'] );
				return $html;
			}

		}


		public function setVal($key,$value)
		{
			$_SESSION[$key] = $value;
		}


		public function getVal($key = null )
		{
			if ($key) {
				if (isset($_SESSION[$key])) {
					return $_SESSION[$key];	
				} else {
					return false;
				}
				
			} else return $_SESSION;
		}

		public function isLogged()
		{
			return isset($_SESSION['User']->id);
		}

		public function getUserRights()
		{
			return $_SESSION['User']->usertype_id;
		}

		public function isLoggedAs()
		{
			return isset($_SESSION['User']->usertype_id);
		}


		public function user($key)
		{
				if ($this->getVal('User')) {
				if (isset($this->getVal('User')->$key )) {
					return $this->getVal('User')->$key;
				} else {
					return false;
				}
			}
			return false;
		}
	}
?>