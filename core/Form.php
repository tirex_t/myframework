<?php
	
	/**
	*  Classe permettant de generer des formulaires
	*/
	class Form 
	{
		public $controller = false;
		public $errors;

		public function __construct($controller)
		{
			$this->controller = $controller;
		}

		public function input($name, $label,$options = array())
		{
			$error = false;
			$classError = '';
			if ( isset($this->errors[$name]) ) {
				$error = $this->errors[$name];
				$classError = ' error';
			}
			if ( !isset($this->controller->request->data->$name )) {
				$value = '';
			} else {
				$value = $this->controller->request->data->$name;
			}


			//Si le label est hidden, on veut avoir un champ caché, utile pour id...
			if ( $label =='hidden' ) {
				if (isset($options['value'])) {
					$value = $options['value'];
				}
				return '<input type="hidden" name="'.$name.'" value="'.$value.'"/>';
			}
			$input  = '<div class="form_group"></br>';
			$input .='<label for="'.$name.'" class="col-sm-6">'.$label.'</label>';

			$attr = '';//Stockage des parametres supplémentaires passés dans le tableau
			foreach ($options as $k => $v) {
				if( $k != 'type' )	{
					if ( is_array($v) ) {
						$attr[$k] = $v;
					} else 	$attr .=" $k =\"$v\"";	
				}

			}
			if (!isset($options['type'])) {
				$input.= '<input type="text" class="form-control" id="input'.$name.'" name="'.$name.'" value="'.$value.'" '.$attr.'/>';
			} elseif($options['type'] == 'textarea') {

				$input.= '<textarea id="input'.$name.'" class="form-control" name="'.$name.'" '.$attr.'>'.$value.'</textarea>';
			}elseif($options['type'] == 'checkbox') {

				$input.= '<input type="hidden" name="'.$name.'" value="0"><input type="checkbox" name="'.$name.'" value="1" '.empty($value)?'':'checked'.'>';
			}elseif($options['type'] == 'file') {

				$input.= '<input type="file" class="input-file" id="input" name="'.$name.'" '.$attr.'>';
			}elseif($options['type'] == 'password') {

				$input.= '<input type="password" id="input'.$name.'" name="'.$name.'" value="'.$value.'"'.$attr.'>';

			}elseif ($options['type'] == 'select' ) {
				$input.='<select name="'.$name.'">';
				foreach ($attr['options'] as $opt ) {
					$selected = ($opt->id == $value) ? 'selected':'';
					$input.='<option value="'.$opt->id.'" '.$selected.'>'.$opt->libelle.'</option>';
				}
				$input.='</select>';
			} elseif ($options['type'] =='radio') {
				foreach ($attr['options'] as $opt) {
					$selected = ($opt == $value) ? 'checked':'';
					$input.= '<input type="radio" name="'.$name.'" value="'.$opt.'" '.$selected.'>'.$opt.'  ';
				}
			}
			$input.= '</div>';

			return $input;
		}

		private function createThumb($images_url )
		{
			$input = '<div>';
			foreach ($images_url['urls'] as $img) {
				$input.='<div class="col-md-1">';
				$input.='<a href="#" class="thumbnail"><img src="'.$img.'" alt=""></a></div>';
			}
			$input.= $this->createAjaxFileUploader();
			// $input.= '<span class="btn btn-success fileinput-button">';
			// $input.= '<i class="glyphicon glyphicon-plus"></i>';
			// $input.='<span>Ajouter des images</span>';
			// $input.='<input id="fileupload" type="file" name="files[]" multiple>';
			$input.="</div>";
			return $input;
		}


		private function createAjaxFileUploader()
		{
			// $text = '<div id="mulitplefileuploader">Upload</div>';
			$text = '<a id="uploadbtn" href="">Ajouter des images</a>';
			return $text;
			//Crée et retourne un formulaire d'upload
		}
	}
?>