<?php
	
	/**
	* Permet de recuperer l'Url et de traiter la requete 
	*/
	class Dispatcher	{

		/**
		* core/Request
		*
		**/
		public $request;
		
		function __construct()
		{
			$this->request = new Request();
			Router::parse( $this->request->url,$this->request );
			//la propriete request est maintenant chargée des bons params, on continue
			//Controleur par défaut
			$controller = $this->loadController();
			$action = $this->request->action;
			if ($this->request->prefix && $this->request->prefix!='admin') {
				$action = $this->request->prefix.'_'.$action;
			}


			if (!in_array($action, array_diff(get_class_methods($controller),get_class_methods("Controller")) )) {
				
				$this->error('Le controleur '.$this->request->controller.' n\'a pas de methode: '.$this->request->action.'');
			}
			try {
				call_user_func_array(array($controller,$action), $this->request->params);
			} catch (Exception $e) {
				echo $e->getMessage();
			}			
			$controller->render($action);
		}

		function error($message)
		{

			header("HTTP/1.0 404 Not Found");
			$controller = new Controller($this->request);
			$controller->set("message",$message);
			$controller->renderError();
		}

		function loadController()
		{

			$name = ucfirst($this->request->controller).'Controller';			
			$file = ROOT.DS.'controllers'.DS.$name.'.php';
			require($file);
			$controller = new $name($this->request);
			return $controller;
		}
	}
?>