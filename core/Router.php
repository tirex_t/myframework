<?php

	/**
	*  Classe qui recupere l'url demandée et definit le controller à utiliser
	*  Merci à grafikart pour les ameliorations
	*/
	class Router
	{

		static $routes = array();
		static $prefixes = array();




		static function prefix($url,$prefix)
		{
			//Fonction me permettant de definir un prefixe
			self::$prefixes[$url] = $prefix;
		}
		/**
		 * permet de parser une Url
		 * @param $url Url à parser
		 * @return tableau contenant les parametres
		 */


		public static function parse( $url,$request )
		{
			$urls = explode('.', $url);
			$url = $urls[0];
			$url = trim($url,'/');
			if (empty($url)) {
					$url = Router::$routes[0]['url'];
				} else{
					$match = false;
					foreach ( ROUTER::$routes as $v ) {
						if ( !$match) {
							if (preg_match($v['redirreg'].'/', $url,$match)) {
								vexp($v['redirreg'],true);
								$url = $v['redir'];
								foreach ( $match as $k => $val ) {
									$url = str_replace(':'.$k.':', $val, $url );
								}
								$match = true;
							}
	
						}
					}
				}

			
			$params = explode('/', $url);

			if (in_array($params[0], array_keys(self::$prefixes))) {
				$request->prefix = self::$prefixes[$params[0]];
				array_shift($params);
			}

			if ($request->prefix =='admin') {
				// $request->controller = $params[0];
				$request->controller = 'backend';
			} else{
				$request->controller = "pages";	
			}
			// $request->controller = $params[0];
			//$request->action = $params[0];
			//Si param 0 = pages et param 1 défini, on teste params 1
			if (isset($params[0]) && $params[0]!='pages') {
				$request->action = isset($params[0])? $params[0] : 'index';
				$request->params = array_slice($params,1);
			}else {
				$request->action = isset($params[1])? $params[1] : 'index';
				$request->params = array_slice($params,2);
			}
			foreach (self::$prefixes as $k => $v ) {
				if ( strpos($request->action, $v.'_' ) === 0 ) {
					$request->prefix = $v;
					$request->action = str_replace($v.'_', '', $request->action);
				}
			}
			return true;
		}


		public static function connect( $redir,$url )
		{	

			$r = array();
			$r['params'] = array();
			$r['url'] = $url;

			$r['originreg'] = preg_replace('/([a-z0-9]+):([^\/]+)/', '${1}:(?P<${1}>${2})', $url );
			$r['originreg'] = '/^'.str_replace('/', '\/', $r['originreg']).'$/';

			$r['origin'] = preg_replace('/([a-z0-9]+):([^\/]+)/', '${1}:', $url );
			$r['origin'] = str_replace( '/*', ':args:', $r['origin'] );

			$params = explode('/',$url);
			foreach ($params as $k => $v) {
				if ( strpos($v, ':') ) {
					$p = explode( ':', $v );
					$r['params'][$p[0]] = $p[1];
				}
			}

			$r['redirreg'] = $redir;
			$r['redirreg'] = str_replace('/*', '(?P<args>/?.*)', $r['redirreg']);
			foreach ($r['params'] as $k => $v) {
				$r['redirreg'] = '/^'.str_replace(":$k:", "(?P<$k>$v)", $r['redirreg']);
			}
			$r['redirreg'] = '/'.str_replace('/', '\/', $r['redirreg'].'$/');
			$r['redir'] = preg_replace('/:([a-z0-9]+)/', ':${1}:', $redir);
			$r['redir'] = str_replace('/*', ':args:', $r['redir']);
			self::$routes[] = $r;
		}



		public static function url( $url = '' )
		{
			$urls = explode('.', $url);
			$url = $urls[0];
			trim($url,'/');
			foreach (self::$routes as $route) {
				if (preg_match($route['originreg'], $url,$match)) {
					foreach ($match as $k => $route) {
						// $url = $route['redir'];
						foreach ( $match as $k => $v ) {
							
							$url = str_replace(":$k:", $v, $url);
						}
					}

					foreach ( self::$prefixes as $k => $v ) {
						$url = str_replace( $v,$k,$url );
					}
					return PROJECT_URL.$url; 
					// return BASE_URL.'/'.$url;
				}
			}
			//On verifie si le prefixe se trouve dans l'Url
			foreach (self::$prefixes as $k => $v ) {
				if (strpos($url, $v)===0 ) {
					$url = str_replace($v, $k, $url);
				}
			}
			return PROJECT_URL.$url;
		}


		public static function webroot($url)
		{
			trim($url,'/');
			return BASE_URL.'/'.$url;
		}
	}
?>