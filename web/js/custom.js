$(document).ready(function(){
  $(".empty:empty").remove();
  $("h3.title:empty").remove();


    $("#slider").flexslider({
        controlNav: true,
        directionNav: true,
        controlsContainer: '.details > div',
        manualControls: '#slidenav li',
        animationLoop: true,
        animation: "slide",
        useCSS: true,
        smoothHeight: true,
        touch: true,
        slideshow: true,
        slideshowSpeed:3000,
        pauseOnAction: false,
        animationSpeed: 600,
        before: function(slider){
          var detailItems = $('.details .items li', slider),
              nextItem = detailItems.eq(slider.animatingTo);
           detailItems.not(nextItem).fadeOut();
           nextItem.fadeIn();
        }
  });

  $('#links').click(function(e){
          e = e || window.e;
    var target = e.target || e.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, e: e},
        links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
  });

  $("#magalerie").justifiedGallery();


  $('#input_1_2').datetimepicker({
    // yearOffset:222,
    lang:'fr',
    timepicker:false,
    format:'d/m/Y',
    formatDate:'d/m/Y',
    minDate:'-1970/01/02' // yesterday is minimum date
    // maxDate:'+1971/12/12' // and tommorow is maximum date calendar
  });


  $('#input_1_1_1').datetimepicker({
    // yearOffset:222,
    lang:'fr',
    datepicker:false,
    timepicker: true,
    step: '30',
    allowTimes:[
      '12:00',
      '12:30',
      '13:00',
      '13:30',
      '14:00',
      '15:00',
      '16:00',
      '17:00',
      '18:00',
      '18:30',
      '19:00',
      '19:30',
      '20:00',
      '20:30',
      '21:00',
      '21:30',
      '22:00',
      '22:30',
    ]
  });

  $('.fancy').fancybox();

  //  $("#jquery_jplayer_1").jPlayer({
  //   ready: function () {
  //     $(this).jPlayer("setMedia", {
  //       title: "Bubble",
  //       m4a: "http://www.jplayer.org/audio/m4a/Miaow-07-Bubble.m4a",
  //       oga: "http://www.jplayer.org/audio/ogg/Miaow-07-Bubble.ogg"
  //     });
  //   },
  //   swfPath: "/js",
  //   supplied: "m4a, oga"
  // });

 $("#jquery_jplayer_1").jPlayer({
    ready: function () {
      $(this).jPlayer("setMedia", {
        title: "Bubble",
        m4a: "http://www.jplayer.org/audio/m4a/Miaow-07-Bubble.m4a",
        oga: "http://www.jplayer.org/audio/ogg/Miaow-07-Bubble.ogg"
      });
    },
    swfPath: "/js",
    supplied: "m4a, oga"
  });

});
