$(document).ready(function(){

    var progressbox     = $('#progressbox');
    var progressbar     = $('#progressbar');
    var statustxt       = $('#statustxt');
    var btnSubmit       = $(".btnsubmit");
    var myform          = $("#form_upload");
    var output          = $("#output");
    var completed       = '0%';

    $(myform).ajaxForm({
        beforeSend: function() { //brfore sending form
            btnSubmit.attr('disabled', ''); // disable upload button
            statustxt.empty();
            progressbox.slideDown(); //show progressbar
            progressbar.width(completed); //initial value 0% of progressbar
            statustxt.html(completed); //set status text
            statustxt.css('color','#000'); //initial color of status text
        },
        uploadProgress: function(event, position, total, percentComplete) { //on progress
            progressbar.width(percentComplete + '%') //update progressbar percent complete
            statustxt.html(percentComplete + '%'); //update status text
            if(percentComplete>50)
                {
                    statustxt.css('color','#fff'); //change status text to white after 50%
                }
            },
        complete: function(response) { // on complete
            output.html(response.responseText); //update element with received data
            myform.resetForm();  // reset form
            // btnSubmit.removeAttr('disabled'); //enable submit button
            progressbox.slideUp(); // hide progressbar
            var url = $(location).attr('href');
            window.location.href= "http://www.mvcphp.dev/cockpit/"; 
        }
    });

    $('.sousmenu_type').click(function(){
        var texte = $(this).text();

        if ($(this).hasClass('sousmenu_selected')) {
            $(this).removeClass('sousmenu_selected');
            $("label[for='type']").text("Nouvelle catégorie:");
            $("#form_type input[name=id]").val();
            $('input[name=type]').val('');
            $("button[type=submit]").text("Enregistrer");
        } else {
            $(this).addClass('sousmenu_selected');
            $("label[for='type']").text("Edition d'une categorie:");
            $('input[name=type]').val(texte);
            $("#form_type input[name=id]").val($(this).attr('rel'));
            $("button[type=submit]").text("Mettre à jour");
        }
        return false;
    });

    $(".alert").alert();
        window.setTimeout(function() {
        $(".alert").fadeTo(300, 0).slideUp(300, function(){
            $(this).remove(); 
        });
    }, 5000);


    $('.table-users tbody tr').click(function(){
        $('.table-users tbody tr').each(function(){
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            };
        })
        $(this).addClass('selected');
        return false;
    });

    $('#add_zik').click(function(e){
        $('#uploadzik').slideToggle("fast");
        return false;
    });


    $(".confirm_user").click(function(e){
        var text = $('.hidden_for_alert').html();
        $('.confirm_user').avgrund({            
            width: 580, // max is 640px
            height: 200, // max is 350px
            showClose: true, // switch to 'true' for enabling close button 
            showCloseText: 'Fermer', // type your text for close button
            closeByEscape: true, // enables closing popup by 'Esc'..
            closeByDocument: true, // ..and by clicking document itself
            holderClass: '', // lets you name custom class for popin holder..
            overlayClass: '', // ..and overlay block
            enableStackAnimation: false, // another animation type
            onBlurContainer: '', // enables blur filter for specified block 
            template: $('#hidden_for_alert').html()
        });
        return false;
    });

   $('#musicfile').change(function() {
        var filename = $('#musicfile').val();
        $('#select_file').html(filename);
    });

});