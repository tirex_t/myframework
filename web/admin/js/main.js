$(document).ready(function(){
	var uploader = new plupload.Uploader({
		runtimes: 'html5,flash',
		container: 'plupload',
		drop_element: 'droparea',
		browse_button: 'browse',
		url:'/cockpit/upload_media',
		multipart: true,
		rename : true,
		sortable: true,
		canOpenDialog: true,
		prevent_duplicates: true,
		urlstream_upload : true,
		dragdrop : true,
        // Flash settings
        flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
        // Silverlight settings
        silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',
		filters: [
			{title: 'Images',extensions : 'jpg,gif,png,jpeg'},
			{ title : "multimedia files", extensions : "mp3" }
		]
	});

	uploader.init();

	// Evenement qui se déclenche lorsqu'on ajoute un fichier à la liste
	uploader.bind('FilesAdded',function(up,fichiersAjoutes){
		//Parcourt la liste des fichiers ajoutés et ajoute leur nom et leur poids à la liste 
		var filelist = $('#filelist');
		for (var i in fichiersAjoutes) {
			 var f = fichiersAjoutes[i];
			 var img = new mOxie.Image();
			 //Apercu de l'image
			 filelist.prepend('<div id="'+f.id+'" class="pl-file">'+f.name+' ('+plupload.formatSize(f.size)+')<div class="progressbar"><div class="progress"></div></div></div>');
		};
		$('#droparea').removeClass('survol-upload');
	});

	//Evenement declenché lors de l'upload d'un fichier
	uploader.bind('UploadProgress',function(up,file){
		$('#'+file.id).find('.progress').css('width',file.percent+'%');
	});

	//Gestion des erreurs
	uploader.bind('Error',function(up,error){
		alert(error.message);
		up.refresh();
		$('#droparea').removeClass('survol-upload');
	});

	uploader.bind('FileUploaded',function(up,file,response){
		if (response.error) {
			alert(data.message);
			$('#'+file.id).remove();
		}else{
			$('#'+file.id).find('.progressbar').fadeOut();
		};
	});

//Survol de la zone de glisser-deposer
	jQuery(function($){
		$('#droparea').bind({
			dragover :  function(e){
				$(this).addClass('survol-upload');
			},
			dragleave :  function(e){
				$(this).removeClass('survol-upload');
			}
		});

	});

	//Evenement se déclenchant lorsqu'une suppression est effectuée
	uploader.bind('FilesRemoved', function(up, files) {
	  if (up.files.length < up.settings.max_files) {
	    $(up.settings.browse_button).show();
	  }
	});


    $('#formulaire .btn_emmo_submit').click(function(e) {
    	//On verifie si le formulaire est complet avant validation
			uploader.start();
		if (uploader.files.length>0) {
            var obj = uploader.files,
                images_url = [];
            $.each(obj, function(key, val) {
                images_url.push(val.name);
            });

            uploader.bind('StateChanged', function(uploader) {
			    if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
	                $('#formulaire').append('<input type="hidden" name="images_url" value="'+images_url.join(',')+'" /> ');               
			        $('form')[0].submit();
			    }
			});
		}else {
            alert("Vous devez ajouter au moins un media à la liste.");
        }
        // uploader.refresh();
        // if ($('#uploader').plupload('getFiles').length > 0) {
        //     var obj = $('#uploader').plupload('getFiles'),
        //         images_url = [];
        //     $.each(obj, function(key, val) {
        //         images_url.push(val.name);
        //     });
        //     alert(images_url);
        //     $('#uploader').on('complete', function() {
        //         $('#formulaire').append('<input type="hidden" name="images_url" value="'+images_url.join(',')+'" /> ');               
        //         // $('#formulaire').submit();
        //     });
        // } else {
        //     alert("Vous devez ajouter au moins un media à la liste.");
        // }
        return false; // Keep the form from submitting
    });

	$(document).on('click','.del',function(e){
		e.preventDefault();
		if (confirm('Voulez-vous vraiment supprimer ce media de la liste ?')) {
			// $.get('/cockpit/delete_media',{file:$(this).attr('href')});
			$.ajax({
				url: '/cockpit/delete_media',
				data: $(this).attr('href')
			});
		};
	});
	
});