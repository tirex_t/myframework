$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
$(function() {
    $(window).bind("load resize", function() {

        if ($(this).width() < 768) {
            $('div.sidebar-collapse').addClass('collapse')
        } else {
            $('div.sidebar-collapse').removeClass('collapse')
        }
    })
})

$(document).ready(function(){

    function readURL(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
            $('#img_prev')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
            };
            reader.readAsDataURL(input.files[0]);
            };
        }

        $('input[name=file]').change(function(){
            readURL(this);
        });

        $("area[rel^='prettyPhoto']").prettyPhoto();
        
        $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
            animation_speed:'normal',
            theme:'light_square',
            slideshow:3000, 
            autoplay_slideshow: true
        });

        $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({
            animation_speed:'fast',
            slideshow:10000, 
            hideflash: true
        });

        $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
            custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
            changepicturecallback: function(){ initialize(); }
        });

        $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
            custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
            changepicturecallback: function(){ 
                _bsap.exec();
            }
        });


        $('.thumbnails a').click(function(){
            var selected = $(this).attr('rel');
            //On écrit la valeur dans un champ caché
            if ($(this).hasClass("selected_image")) {
                $(this).removeClass("selected_image");
            } else {
                $(this).addClass("selected_image");    
            }
            updateSelectedImages(selected);
            return false;
        });

        function updateSelectedImages(value)
        {
            var arrImages = $("input[name$='image_urls']").val(),
                arrImg = arrImages.split(";"),
                index = $.inArray(value,arrImg);
            if (index == -1) {
                arrImg.push(value);
            }else{
                arrImg.splice(index,1);
            }
            $("input[name$='image_urls']").val(arrImg.join(";"));
        }

});
