$(function (){  
	  
	$.fn.googleMapAuocompleteAddress = function(opt) {  
		var options = $.extend({}, {}, opt);   
		return this.each(function(i, e){ 
			if(! $(this).is("input"))return ;
            var from=$(this).closest("form");			
			var autocomplete = new google.maps.places.Autocomplete($(this).get(0));  
			google.maps.event.addListener(autocomplete, 'place_changed', function() {  
				var place = autocomplete.getPlace();  
				var componenents=place.address_components||[];  
				var ln=componenents.length; 
				from.find("input[data-role]").val("");			
				for (var i=0 ; i<ln;i++) {   
					var editor=$("[data-role|="+componenents[i].types[0]+"]");   
					if(editor.is("input")){  
						editor.val(componenents[i].long_name);  
					}else{  
						editor.html(componenents[i].long_name);  
					}//end if   
				}//end for   

			});//end addListener   
		});//end each    
	};//end googleMapAuocompleteAddress  
});  