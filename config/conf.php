<?php

	/**
	* 
	*/
	class Conf 
	{
		public static $debug = 2;
		
		static $databases = array(
			'default' =>array(
				'host' => 'localhost',
				'database' => 'tests',
				'user' => 'tests',
				'passwd' => 'tests'
			)
		);

		static $mailConf = array(
				'adminMail' => 'bonaprethy@gmail.com',
				'adminName' => 'Test nomadministrateur'
			);

		static $imagesConf = array(
			'upload_path'   => 'upload',
			'gallery_path'  => 'images/gallery',
			'fullsize_path' => 'upload/fullsize',
			'thumb_path'    => 'upload/thumbnails',
			'images_menus'  => 'upload/images_menus',
			'images_slider' => 'upload/images_slider',
			'docs_user'  	=> 'upload/docs_user',
			'music'  		=> 'upload/music',
			'thumb_width'   => '200',
			'thumb_height'  => '80'
		);

		static $analytics = array(
			'ClientID' => '399884004431.apps.googleusercontent.com',
			'EmailAddress' => '399884004431@developer.gserviceaccount.com',
			'Clientsecret' => '',
			'RedirectUris' => '.com'
  		);

  		static $parPage = array(
  			'formations' 	=> 10,
  			'intervenants'    => 10
  		);
		
	}
 	
?>