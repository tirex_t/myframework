<?php 

 	// Router::prefix('cockpit', 'admin');
 	Router::connect('/','pages/index');
 	Router::connect('/contact','pages/contact');
 	Router::connect('/location','pages/location');
 	Router::connect('/login','pages/login');
 	Router::connect('/formations/:id','pages/location/:id');
 	Router::connect('blog/:action','articles/:action');
 ?>