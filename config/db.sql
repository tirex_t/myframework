SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `tb_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `tb_db` ;

-- -----------------------------------------------------
-- Table `tb_db`.`User_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tb_db`.`User_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `libelle` VARCHAR(45) NULL,
  `droits` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_db`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tb_db`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `prenom` VARCHAR(255) NULL,
  `societe` VARCHAR(255) NULL,
  `telephone` VARCHAR(45) NULL,
  `adresse` VARCHAR(500) NULL,
  `usertype_id` INT NOT NULL,
  `datecreation` DATETIME NOT NULL,
  `datemodif` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `user_type_idx` (`usertype_id` ASC),
  CONSTRAINT `user_type`
    FOREIGN KEY (`usertype_id`)
    REFERENCES `tb_db`.`User_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_db`.`User_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tb_db`.`User_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` VARCHAR(45) NULL,
  `server_infos` VARCHAR(45) NULL,
  `dureeconn` VARCHAR(1000) NULL,
  `datecreated` DATETIME NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `user_key`
    FOREIGN KEY (`id`)
    REFERENCES `tb_db`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- Ajout de la table Slider pour la gestion des sliders

-- --------------------------------------------------------

--
-- Structure de la table `Slider`
--

CREATE TABLE IF NOT EXISTS `Slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  `legend` varchar(300) NOT NULL,
  `datecreated` datetime NOT NULL,
  `datemodif` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



-- Ajout d'un type d'user
INSERT INTO `tb_db`.`user_type` (
  `id` ,
  `libelle` ,
  `droits`
)
VALUES (
  NULL , 'admin', '1'
);

INSERT INTO `tb_db`.`user` (
`id` ,
`email` ,
`password` ,
`nom` ,
`prenom` ,
`societe` ,
`telephone` ,
`adresse` ,
`usertype_id` ,
`datecreation` ,
`datemodif`
)
VALUES (
NULL , 'admin@admin.fr', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'administrateur', 'prenomadmin', NULL , NULL , NULL , '1', '2014-02-16 21:00:00', '2014-02-16 21:00:00'
);

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `nom`, `prenom`, `societe`, `telephone`, `adresse`, `usertype_id`, `datecreation`, `datemodif`) VALUES
(2, 'admin@admin.fr', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'administrateur', 'prenomadmin', NULL, NULL, NULL, 1, '2014-02-16 21:00:00', '2014-02-16 21:00:00');

ALTER TABLE `user_log` DROP FOREIGN KEY `user_key` ;

ALTER TABLE `user_log` ADD INDEX ( `user_id` ) ;

ALTER TABLE `user_log` CHANGE `user_id` `user_id` INT( 11 ) NOT NULL ;

ALTER TABLE `user_log` ADD FOREIGN KEY ( `user_id` ) REFERENCES `tb_db`.`user` (
`id`
) ON DELETE NO ACTION ON UPDATE NO ACTION ;



-- Structure de la table `Media`
--

CREATE TABLE IF NOT EXISTS `Media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `thumb_url` varchar(100) DEFAULT NULL,
  `datecreated` datetime NOT NULL,
  `datemodif` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

 -- Création de droits supplementaires
INSERT INTO `tb_db`.`user_type` (`id`, `libelle`, `droits`) VALUES (NULL, 'Enregistré', '2'), (NULL, 'Attente', '3');