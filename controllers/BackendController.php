<?php 
	class BackendController extends Controller
	{
		/**
		 *  L'utilisateur
		 */
		public $user;

		public function index()
		{
			$this->user = $_SESSION['User'];
			$this->currentPage = "Page d'administration";

			
			//On charge les derniers objets saisis pour le backoffice

			$this->set('currentPage',$this->currentPage);

			$this->render('index');	
		}

		/**
		 * Admin- Modifier horaires d'ouverture 
		 *
		 */
		function catmenu_index()
		{
			$data = $this->request->data;
			if ($this->request->data) {
				$this->loadModel('CatMenu');
				$this->CatMenu->save($data);
				$data = $this->request->data;
				vexp($data,true);exit;

			}

			$this->render('catmenu_index');	
		}

		function admin_edit( $id = null)
		{

		}		

		function music_edit( )
		{
			$this->loadModel('Media');
			if ( $this->request->data ) {
				//On uploade la chanson
				$data = $this->request->data;
				vexp($data,true);exit;
				$this->Media->uploadMusic($data);
			}
			$music = $this->Media->getAllMusic();
			$this->set('music',$music);
			$this->set('breadcrumb',"Gestion des musiques");
			$this->render('music_edit');
		}		



		//Locations

		public function menu_index()
		{
			$this->currentPage = "Menus";
			$this->set('menus',$menus );
			$this->render('menu_index');
		}

		public function menu_edit()
		{
			$this->loadModel('CatMenu');
			$typesMenus = $this->CatMenu->getAllCatMenus();
			$this->set('typesmenus',$typesMenus);
			$this->render('menu_edit');
		}


		public function plat_edit()
		{
			$data = $this->request->data;
			if ( $data ) {
			}
			$this->loadModel('CatMenu');
			$types = $this->CatMenu->getAllCatMenus();
			$this->set( 'typesmenus',$types );
			$this->render('plat_edit');
		}




		function resa_index()
		{
			$this->loadModel('Reservation');
			$resas = $this->Reservation->findAll();
			$this->currentPage = "Reservations";
			$this->set('resas',$resas );
			$this->render('resa_index');
		}

		function produit_index()
		{
			$this->render('produit_index');
		}

		function produit_edit( $id = null )
		{
			$this->render('produit_edit');
		}


		//Upload d'images
		public function uploadImage( )
		{
		  $conf = Conf::$imagesConf;
		  $handle = new Upload($_FILES['image_field']);
		  $thumbs = clone $handle; //Images à redimensionner

		  $handle->process($conf['fullsize_path']);
		  if ( $handle->processed ) {
		  	//on sauvegarde les thumbnails
		  	$thumbs->file_new_name_body ='thumb';
		  	$thumbs->image_resize = true;
		  	$thumbs->image_x = $conf['thumb_width'];
		  	$thumbs->image_ratio_y = true;
		  	$thumbs->process($conf['thumb_path']);
		  	if ($thumbs->processed) {
		  		echo 'ok';
		  	}
		  	exit;
		  }
		}


		function user_index()
		{
			$this->render('user_liste');
		}

		function slider_index()
		{
			$this->render('slider_edit');
		}

		public function media_edit( $id = null)
		{
			$this->loadModel('Media');
			if ( !$id ) {
				# code...
			} else {
				//Upload 
			}
			$this->render('media_edit');
		}

		public function media_upload( )
		{
		}
	}
 ?>