<?php
	/**
	* 
	*/
	class PagesController extends Controller
	{
		private $currentPage ='';
		
		function index()
		{
			$this->loadModel('Intervenants');

			// $intervenants = $this->Intervenants->findAll();
			$this->currentPage = $this->request->action;
			$this->set('currentPage',$this->currentPage);
			$this->render('index');
		}



		function contact()
		{
			$this->currentPage = $this->request->action;
			$this->set('currentPage',$this->currentPage);
			$this->render('contact');
		}


		public function menu()
		{
			$this->loadModel('CatMenu');
			$catmenus = $this->CatMenu->findAll();
			$this->currentPage = $this->request->action;
			$this->set('currentPage',$this->currentPage);
			$this->set('catmenu',$catmenus);
			$this->render('menu');	
		}


		function reservation()
		{			
			$this->currentPage = $this->request->action;
			if ( $this->request->data ) {
				$this->loadModel('Reservation');
				$data = $this->request->data;
				$this->Reservation->save( $data );
				$this->Session->flash('success');
				//Reservation postée, on l'enregistre et on envoie un mail.
				// $this->sendMail('reservation');
				// echo "ok";
				// exit;
			}
			$this->set('currentPage',$this->currentPage);

			$this->render('reservation');
		}

		function galerie()
		{
			$this->currentPage = $this->request->action;
			$this->loadModel('Media');
			$galerie = $this->Media->getGallery();
			$this->set('currentPage',$this->currentPage);
			$this->set('galerie',$galerie);
			$this->render('galerie');
		}

		public function login()
		{
			if ($this->request->data) {
				//On teste ici les infos de connexion
				$data = $this->request->data;
				$data->password = sha1($data->password);
				$this->loadModel('Users');
				$user = $this->Users->findFirst(array(
						'conditions' => array( 'email' => $data->email,'password' => $data->password)
					));
				if (!empty($user)) {
					//Utilisateur connecté, on regarde ses droits et on l'oriente sur la page
					//On déclenche ici le timer
					$rights = $this->Session->getUserRights();
					
					$this->Session->setVal('user_rights',$rights);
				} else {
					$error = "Utilisateur inconnu";
				}
				$this->request->data->Mot_passe_user = '';//Evite de renvoyer la clé à l'affichage du formulaire en cas d'erreur
			}


			if ($this->Session->isLogged()) {
				if ($this->Session->getVal('user_rights') == 1) { //On teste les droits de l'utilisateur et on le redirige si ok vers la page admin
					$this->redirect('/cockpit/index');	
				} elseif ($this->Session->user('rights_id') == 2 ) {
					$this->redirect('/admin/index');	
				}
				
			}

			$this->currentPage = $this->request->action;
			$this->set('currentPage',$this->currentPage);
			$this->render('login');			
		}

		public function logout()
		{
			unset($_SESSION['user_rights']);
			$this->Session->setFlash("Vous etes bien déconnecté");
			$this->redirect('/');
		}

	}
?>